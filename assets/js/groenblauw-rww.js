/* global $ */
/* global jQuery */
/* global console */
/* global mapboxgl */
/* global google:true */
/* global InfoBox:true */
/* global proj4:true */
/* global nlmaps:true */
/* global MarkerClusterer:true */
/* global gbrww_ajaxURL */
/* global gbrww_ipAddress */
/* global gbrww_themes */

/* global map */
/* global addressID */
/* global photoID */
/* global addressInfo */
/* global locations */
/* global takenActions */
/* global isSet */

var gbrww_bbox_extent = [-Math.PI * 6378137, Math.PI * 6378137];
var gbrww_hv_gbrww_st_markers = [];
var gbrww_hv_mainMarker, gbrww_hv_map;
var gbrww_hv_showTooltip = false;
var gbrww_browserWidth;
var gbrww_browserHeight;
var gbrww_hv_locations = [];
var gbrww_infobox;

var gbrww_st_colors = ['#33a357', '#79b752', '#c3d545', '#fff12c', '#edb731', '#d66f2c', '#cc232a'];

var gbrww_st_buildingLabels;
var gbrww_st_currentBuildingCenter = '';
var gbrww_st_buildingColorRule;
var gbrww_st_markers = [];
var gbrww_st_themeScores = {};
var gbrww_st_currentBuildingOutline;
var gbrww_st_buildingID, gbrww_st_labelNum, gbrww_st_addressPostcode, gbrww_st_addressHouseno, gbrww_st_allActions, gbrww_st_map;

function gbrww_get_browser_size() {

    var e = window, a = 'inner';
    if (!('innerWidth' in window )) {
        a = 'client';
        e = document.documentElement || document.body;
    }

    gbrww_browserWidth = e[a+'Width'];
    gbrww_browserHeight = e[a+'Height'];
}

function gbrww_is_postcode_buiten_gemeente(postcode) {
		    
	var vierCijfers = parseInt(postcode.substr(0, 4));
	
    var min = 3000;
    var max = 3199;
	
	var buitenGemeente = vierCijfers < min || vierCijfers > max ? true : false;	

	return buitenGemeente;
}

function gbrww_st_convert_numeral_label_to_char(num) {

	var character;

	if (num === null || num > 6 || num === '') {
		character = 'g';
	} else {
		character = String('abcdefg').charAt(num);
	}

	return character;
}

function gbrww_sanitize_postcode(postcode) {

	postcode = postcode.replace(/\s/g, '').toUpperCase();

	var valid = true;

	if (postcode.length === 6) {

		for (var i=0; i<6; i++) {

			if ( (i<4 && !postcode[i].match(/[0-9]/i)) || (i>=4 && i<6 && !postcode[i].match(/[A-Z]/i))) {
				valid = false;
			}

		}

	} else {
		valid = false;
	}

	return valid ? postcode : false;
}

function gbrww_sanitize_houseno(houseno) {

	houseno = houseno.replace(' ', '').toLowerCase();

	var valid = true;

	for (var i=0; i<houseno.length; i++) {

		if (!houseno[i].match(/[0-9-a-z]/i)) {
			valid = false;
		}

	}

	return valid ? houseno : false;
}

function gbrww_hv_sort_by_distance (a, b) {
	return ((a.distance < b.distance) ? -1 : ((a.distance > b.distance) ? 1 : 0));
}

function gbrww_hv_find_nearest(lat, lng) {
	    							    
    var newCenter = new google.maps.LatLng(lat, lng);

	gbrww_hv_map.setCenter(newCenter);
	
	if (!(lat === 51.921952 && lng === 4.463931)) {
		gbrww_hv_map.setZoom(15);	
	}
									
	if (gbrww_hv_mainMarker !== undefined) {
		gbrww_hv_mainMarker.setMap(null);
	}
	
	gbrww_hv_mainMarker = new google.maps.Marker({
		position: newCenter,
		map: gbrww_hv_map,
		clickable: false
	});
	
	gbrww_hv_mainMarker.setIcon({
		url: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAACXBIWXMAACE3AAAhNwEzWJ96AAAIc0lEQVR4nO1dQY7bNhR9nOkk6CYZoF10MfD4AAVGN4h6grgnqHKCTE4Q5QbuDdQbzKAXUG6gAdJFN4XGnUVRoICdRYo0i98FKftLJiV5TInkjB8gmJYp+ouP/3+K/CIFEeEAf/CVawH6QggRAzgFEKlTccclpTqWAAoAJRGVgwhnEcJHDRFCRJAVXn2eWyp6BUlODiAnotxSudbgDSFCiBlk5c9gj4A+uAZwBeCKiJYj/q8WTgkRQkwBXKIfCbeQJqiANEOlOkyI1WcEYArgoodI1wAyIrrqkXcQOCFE+YNLAC9bsr2HMi0AChutt2EKZwCeG7LeAkjhQmuIaLQDsjJyAGQ4MgAJgNOR5IkAzCE1TSfPEpKYUeQhonEIgTQZJiKKMUnoaCyZQcYSQBI8IZDd1LnhJnMAsUsSWhpPqrRDJ3MUJCGqxelMgZdEGBqTiZg0KEIMWlGGQITmXqaQ3WKdqZ16TYhqVcWYLWpEYnQavwQw85IQ1WNpqncxtM0dmRSTT0y9IkT1kppkZK57TgMSM9PdrxeEKDKaLSZxXWkjkBJpzPPejdA2GcuHZKJ63L/OZxb7kHIgww4xWaMurkYlRNnQAxntpGSjEKLpTT16MlpImQ9KiLKZ5YGM1jrK9+ng7DT8LoTIAbxgp34gD2fdXEIIcQpJSjX/soIcoSj6XH+0wx+lqJPx5kDGNkjOn8wgiQDknEumiOpVQB81jGCpF/FYDmx3fHr5k76F8752iQf6BD4AKc1hlnhvQiCHoHcq9HDU6o835qIrf6sPYUEIFX6mg9/YFQlLXyhfbERrL0sIkQH4SX1dQY7/Ow+VqaCCJSLI7niFArIlli5k0kEIMQfwWn1tr8cWVZvCwwFDJVcG/UweP3KPZD5tyJsa87YUkmEH2zfSjZnm57uIcf7wirovXsLQMWprhd44cphnIvse1mf27nkfZZeWmC70RjsskOFTw0q6tMRUAdzeJY5vQhdgsI+mTB03rta67WKxdExG82nXxpE7vifuB7esj+4Cbh5Sx8KXFonwwnRh2z9P+e+1B0P1IMijxDM4gnrGOB+o+GSgcjtB8vnohp3iD95bT+ozlr4htw9Xs+4sXpbdB3OWjvkPbYRkAwnTF1F3lnvjuXo1wRVylr5QlgnANiEvDBe5wNAV1m9+YgBozFZcJdaEKJtd4ZZ6znANCNPLNA8F/C2tuEoc6U7CvXY8BuQsHVcJEyGutQOQr7QNCaej1lSfxjivpng5IVOW9oGQcsjCPTDJQN2PRECdkHWfn/yYhMoHLPt6wLJ3QcnSG0KaDn08eVpxhU3khm1kA5W7K7iWbpmsCuUoonSA5IzavDPj7rghh++hN1CydAxsCIkNmVxjDvsam1gubx+UzRPeagiw1hIedLYvXnnizHWYAjtELrqCqsAY+5PyioiyvQWyiGbXFwiAEKBGyk1HVh1WAH70jQwTgiAEkKQQUQTgFfoRswLwDnK+wRcn3olgCKlARJki5l1LthURnRJRSh7FkfVBcIT0hK+OuxMhExJspbchZEKCMkU66EZIdIRMxxBmYOSuBbgHSmBDSM5+mI4syH1RuhbAAqbNE8FqiOMADFuYsnQOKEJ0T4yBIxSHz+MGlkBdQ9aDeA1n4zNMwymhOPwpSxdAnZCSpV2GyOyCUDTBBB6UuEVIztKhEGJC6VqALmiifLZMVs7SPLPP0JqmQBx+zNJ5lVgToomCCEFLQjZZPEo0rxLNbi8PvYkHFGZIDDUPbw2aoPa8SjQJ4cPUyVACDYwQtCZm6VpQexshtSBgT5G7FuCe4K8g5PyHGiGaIOBkKIkeKzTmqhZZoxs64RkS6xINj9y1AB3g2rH1Do6OEB6gdi6ESIaRywpC8BdrqPjdhJ3aijvbIkQ9oHBfctnM4wtCm55Ffc+SFer1DMA8QZWy9EVAY1uA31qTsvRc16C0hCi79gu/2KpYdtGMbPRSa9QqQNVI+gqGOm2bwk1Z+sJjX1K6FqALyndw06/VDqCFEJ2W9F430C1K1wJokKLuO4wWpyvIIUV9Mcd0P7kGQa2l+TawqMYEX7NTRu0AOnb6JKJSLb71Vp16LYS4cvFCz2RydgLgG8iG8QzACQA8ffrk0+fP//F8LwB8UV//AfAJwGqxuPs4qsAbZCx9Q0RpW+bOrVeJKFWbPlZPl5kQIhqryzmZnD0D8D0kGX3wjKXX10wmZ/8C+H2xuPvTonitUI2ZP5V3PkL0jctKWPoc476B1ErG8fHx38fHRx+Oj48+PHly8mtLOV8DiBTBg0M1Ym6qeq1X2Xtla9Vte8tOvSGiwbvDk8nZd5AzmCcWivtjsbj7zUI5rVDjVQU2jvwGcsGbTqsSzFLjk8nZNwC+hdSWE9RNkw5fAHyE7JR8BPDXYnH3pf2S/bHvUuO7Li10isNi/F11lKO+/FKy0/X3+MMIh+0qTHWTNcgYdrsK9sfNld4ePSkaMrJ7lbOHAMmBFCMZ4255dCBlff+6FVMLuNgUrIWUnR1ZiAd83DavQUpz6e+9hfP1gM8bSzZaTFPI4iGZMISy9WpDYN0q1NYEdkhGjJA2J24Ir2tFJQLcDAYhb9/do0URDhvcuyGE3ZROW7wlRmmEiYh8aJ845k3mBmIK1Utz2iNTGp0ZZCwxUlfexU2biKm6yqORA9kznBtMa+W00zEby07D77ag4rwuAbxsyfYekrwccheBvWco1fx2DEkED1pr4haSiCsb/7sLnBCy/vPNLnAzdL/9ewvZkgvIlluiPcIkVp8RpMm8MObc4BryAc/Z6kFOCeFQU54x+pFjE9eQ3drRtUEHbwjhaJiWGPYIWkFqWA65sUtuqVxr8JIQHZTfOcXmDeG445JSHUtIEkryLGZLh2AIeSz4H1/bGn9YcJEBAAAAAElFTkSuQmCC',
		scaledSize: new google.maps.Size(45, 45)
	});
	
	var markerDistancesBuiten = [];
	var markerDistancesBinnen = [];
	var markerDistancesWater = [];
	var distance, markerPos;
	
	for (var i=0; i<gbrww_hv_gbrww_st_markers.length; i++) {
		
		markerPos = gbrww_hv_gbrww_st_markers[i].getPosition();
		distance = google.maps.geometry.spherical.computeDistanceBetween(newCenter, markerPos);
		
		if (gbrww_hv_locations[i].type.value === 'koele-buitenplek') {
		
			markerDistancesBuiten.push({
				'locationIndex': i,
				'distance': distance
			});	
			
		} else if (gbrww_hv_locations[i].type.value === 'koele-binnenplek') {
			
			markerDistancesBinnen.push({
				'locationIndex': i,
				'distance': distance
			});	
		} else if (gbrww_hv_locations[i].type.value === 'waterpunt') {
			
			markerDistancesWater.push({
				'locationIndex': i,
				'distance': distance
			});	
		}

	}
										
	markerDistancesBuiten.sort(gbrww_hv_sort_by_distance);
	markerDistancesBinnen.sort(gbrww_hv_sort_by_distance);
	markerDistancesWater.sort(gbrww_hv_sort_by_distance);
	
	jQuery('.gbrww-hv__nearest__list').html('');
	
	var nearestBuitenElem = jQuery('#gbrww-hv-nearest-list-buiten');
	var nearestBinnenElem = jQuery('#gbrww-hv-nearest-list-binnen');
	var nearestWaterElem = jQuery('#gbrww-hv-nearest-list-water');
	var loc, title;
	
	for (var j=0; j<3; j++) {
		
		if (markerDistancesBuiten[j] !== undefined) {
		
			loc = gbrww_hv_locations[markerDistancesBuiten[j].locationIndex];
						
			if (loc !== undefined) {
			
				title = loc.title === '' ? '(naamloos)' : loc.title;
				nearestBuitenElem.append('<li><a class="gbrww-hv__nearest__list__link" href="#" data-locationindex="' + markerDistancesBuiten[j].locationIndex + '">' + title + '</a></li>');	
			}	
		}
		
		if (markerDistancesBinnen[j] !== undefined) {
		
			loc = gbrww_hv_locations[markerDistancesBinnen[j].locationIndex];
			
			if (loc !== undefined) {
				title = loc.title === '' ? '(naamloos)' : loc.title;
				nearestBinnenElem.append('<li><a class="gbrww-hv__nearest__list__link" href="#" data-locationindex="' + markerDistancesBinnen[j].locationIndex + '">' + title + '</a></li>');
			}
		}
		
		if (markerDistancesWater[j] !== undefined) {
		
			loc = gbrww_hv_locations[markerDistancesWater[j].locationIndex];
			
			if (loc !== undefined) {
				title = loc.title === '' ? '(naamloos)' : loc.title;
				nearestWaterElem.append('<li><a class="gbrww-hv__nearest__list__link" href="#" data-locationindex="' + markerDistancesWater[j].locationIndex + '">' + title + '</a></li>');
			}
		}
	}
}

function gbrww_hv_highlight_location(marker, location, infobox) {

	var boxContent = '<div class="gbrww__map__infobox__type--' + location.type.value + '"><p class="gbrww__map__infobox__type">' + location.type.label + '</p>';
	
	if ((location.cat !== undefined && location.cat !== '') || (location.cost !== undefined && location.cost !== '')) {
		
		var items = [];
		
		if (location.cat !== undefined && location.cat !== '') {
			items.push(location.cat);
		}
		
		if (location.cost !== undefined && location.cost !== '') {
			items.push(location.cost);
		}
		
		boxContent += '<p class="gbrww__map__infobox__cat">' + items.join(', ') + '</p>';
	}
	
	if (location.title !== '') {
		boxContent += '<h3 class="gbrww__map__infobox__title gbrww__map__infobox__title--' + location.type.value + '">' + location.title + '</h3>';
	}
	
	if (location.type.value !== 'waterpunt') {
		
		boxContent += 'Hoe koel vind jij deze locatie?<div class="gbrww__map__infobox__rating" data-type="' + location.type.value + '" data-lat="' + location.pos.lat + '" data-lng="' + location.pos.lng + '">';

		var starClass;
		
		for (var i=5; i>0; i--) {
			
			if (location.rating !== undefined && location.rating === i) {
				
				if (location.ownrating) {
					
					starClass = ' gbrww__map__infobox__rating__star--own';
					
				} else {
					
					starClass = ' gbrww__map__infobox__rating__star--rated';
				}
				
			} else {
				
				starClass = '';
			}
					
			boxContent += '<a href="#" class="gbrww__map__infobox__rating__star' + starClass + '" data-rating="' + i + '" title="Geef de koelheid van deze locatie ' + i + ' sterren">★</a>';
		}
	
		boxContent += '</div>';
		
	}
	
	boxContent += '</div>';

	infobox.setContent(boxContent);
	infobox.open(gbrww_hv_map, marker);
	
	gbrww_hv_map.setCenter(marker.getPosition());
	gbrww_hv_map.setZoom(16);
	
}

function gbrww_hv_generate_bbox(x, y, z) {
	
	var tileSize = (gbrww_bbox_extent[1] * 2) / Math.pow(2, z);
	var minx = gbrww_bbox_extent[0] + x * tileSize;
	var maxx = gbrww_bbox_extent[0] + (x + 1) * tileSize;
	var miny = gbrww_bbox_extent[1] - (y + 1) * tileSize;
	var maxy = gbrww_bbox_extent[1] - y * tileSize;
	return [minx, miny, maxx, maxy];
}

function gbrww_convert_rd_to_latlng(x, y) {
	
	if (x < 1000) {
		x *= 1000;
	}
	if (y < 1000) {
		y *= 1000;
	}
	
	var invalidX = (x < 0 || x > 290000);
	var invalidY = (y <290000 || y > 630000);
	
	if (invalidX) {
		alert("x must be between 0 and 290,000");
	}
	
	if (invalidY) {
		alert("y must be between 290,000 and 630,000,");
	}
	
	if (!invalidX && !invalidY) {
	  
		var x0  = 155000.000;
		var y0  = 463000.000;
		var f0 = 52.156160556;
		var l0 =  5.387638889;
		var a01=3236.0331637 ; var b10=5261.3028966;
		var a20= -32.5915821 ; var b11= 105.9780241;
		var a02=  -0.2472814 ; var b12=   2.4576469;
		var a21=  -0.8501341 ; var b30=  -0.8192156;
		var a03=  -0.0655238 ; var b31=  -0.0560092;
		var a22=  -0.0171137 ; var b13=   0.0560089;
		var a40=   0.0052771 ; var b32=  -0.0025614;
		var a23=  -0.0003859 ; var b14=   0.0012770;
		var a41=   0.0003314 ; var b50=   0.0002574;
		var a04=   0.0000371 ; var b33=  -0.0000973;
		var a42=   0.0000143 ; var b51=   0.0000293;
		var a24=  -0.0000090 ; var b15=   0.0000291;
		
		var dx=(x-x0)*Math.pow(10,-5);
		var dy=(y-y0)*Math.pow(10,-5);
		
		var df =a01*dy + a20*Math.pow(dx,2) + a02*Math.pow(dy,2) + a21*Math.pow(dx,2)*dy + a03*Math.pow(dy,3);
		df+=a40*Math.pow(dx,4) + a22*Math.pow(dx,2)*Math.pow(dy,2) + a04*Math.pow(dy,4) + a41*Math.pow(dx,4)*dy;
		df+=a23*Math.pow(dx,2)*Math.pow(dy,3) + a42*Math.pow(dx,4)*Math.pow(dy,2) + a24*Math.pow(dx,2)*Math.pow(dy,4);
		var f = f0 + df/3600;
		
		var dl =b10*dx +b11*dx*dy +b30*Math.pow(dx,3) + b12*dx*Math.pow(dy,2) + b31*Math.pow(dx,3)*dy;
		dl+=b13*dx*Math.pow(dy,3)+b50*Math.pow(dx,5) + b32*Math.pow(dx,3)*Math.pow(dy,2) + b14*dx*Math.pow(dy,4);
		dl+=b51*Math.pow(dx,5)*dy +b33*Math.pow(dx,3)*Math.pow(dy,3) + b15*dx*Math.pow(dy,5);
		var l = l0 + dl/3600;
		
		return {"lat": f-0.001, "lng": l-0.0003}; // <-- correctie, anders verschijnen gbrww_st_markers nét op verkeerde plek :-/
		
	}
}

function gbrww_hv_update_rating(e) {
	
	e.preventDefault();

	var starElem = jQuery(this);
	var ratingElem = starElem.parent();
	var rating = parseInt(starElem.attr('data-rating'));
	var lat = parseFloat(ratingElem.attr('data-lat'));
	var lng = parseFloat(ratingElem.attr('data-lng'));
	var type = ratingElem.attr('data-type');
	
	var locationsOfType = [];
	
	var ratingWithIPfound = false;
		
	// eigen rating met laten zien in zwart
	
	ratingElem.children('.gbrww__map__infobox__rating__star').removeClass('gbrww__map__infobox__rating__star--own').removeClass('gbrww__map__infobox__rating__star--rated');
	starElem.addClass('gbrww__map__infobox__rating__star--own');
		
	for (var i = 0; i<gbrww_hv_locations.length; i++) {
		
		if (gbrww_hv_locations[i].type.value === type) {
			locationsOfType.push(gbrww_hv_locations[i]);
		}

		if (gbrww_hv_locations[i].pos.lat === lat && gbrww_hv_locations[i].pos.lng === lng && gbrww_hv_locations[i].type.value === type) {
		
			gbrww_hv_locations[i].rating = rating;
			gbrww_hv_locations[i].ownrating = true;
					
			// checken of er al een rating is
							
			if (gbrww_hv_locations[i].ratings == undefined) {
					
				// zo nee, nieuwe rating array aanmaken met rating
				
				gbrww_hv_locations[i].ratings = [[gbrww_ipAddress, rating]];
				
			} else {
				
				// checken of er al een rating is met dit IP
				
				for (var j = 0; j<gbrww_hv_locations[i].ratings.length; j++) {
					
					if (gbrww_hv_locations[i].ratings[j][0] !== undefined && gbrww_hv_locations[i].ratings[j][0] === gbrww_ipAddress) {
						
						// zo ja, oude rating updaten
						ratingWithIPfound = true;
						gbrww_hv_locations[i].ratings[j][1] = rating;
						
					}
					
				}
				
				if (!ratingWithIPfound) {
					
					// zo nee, nieuwe rating toevoegen
					gbrww_hv_locations[i].ratings.push([gbrww_ipAddress, rating]);
				}
				
			}
		}
	}
		
	var action = {
	    'action': 'ajax_hv_set_option',
	    'type': type,
	    'locations': JSON.stringify(locationsOfType)
	};
	
	jQuery.post(gbrww_ajaxURL, action, function(response) {
		//console.log(response);
	});
}
function gbrww_hv_get_tiles(coordinates, zoom) {

	var url = (
		"https://diensten.rotterdam.nl/arcgis/services/RWW_WMS/wms_koeleplekken_Rotterdam/MapServer/WMSServer?" +
		"&REQUEST=GetMap&SERVICE=WMS&VERSION=1.1.1" +
		"&LAYERS=Koele%20plekken%20buitenruimte" +
		"&FORMAT=image%2Fpng" +
		"&STYLES=" +
		"&TRANSPARENT=TRUE" +
		"&SRS=EPSG:3857&WIDTH=256&HEIGHT=256" +
		"&BBOX=" + 
		gbrww_hv_generate_bbox(coordinates.x, coordinates.y, zoom).join(",")
	);
		
	return url;
}

function gbrww_hv_setup() {
			
	var mapStyle = [
		{ featureType: "all", elementType: "labels", stylers: [ { visibility: "off" } ] },
		{ featureType: "administrative.locality", elementType: "labels", stylers: [ { visibility: "on" } ] },
		{ featureType: "administrative.locality", elementType: "labels.text.fill", stylers: [ {"color": '#aaaaaa' } ] },
		{ featureType: "water", elementType: "geometry", stylers: [ {color: "#B1D6EF" } ] },
		{ featureType: "poi", stylers: [ {visibility: "off" } ] }
	];

	var myMapOptions = {
		center: new google.maps.LatLng(51.921952, 4.463931), // rotterdam
		panControl: false,
		mapTypeControl: false,
		streetViewControl: false,
	    scrollwheel: false,
		styles : mapStyle,
		zoom: 13,
		fullscreenControl: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	gbrww_hv_map = new google.maps.Map(document.getElementById('gbrww-hv-map'), myMapOptions);
	/*
	var gbrww_hv_map_baselayer = nlmaps.googlemaps.bgLayer(gbrww_hv_map, 'grijs');
	gbrww_hv_map.mapTypes.set('kadaster-grijs', gbrww_hv_map_baselayer);
	gbrww_hv_map.setMapTypeId('kadaster-grijs');
	*/
	/*
	var koelevlakken = new google.maps.ImageMapType({
		getTileUrl: gbrww_hv_get_tiles,
		name: "Koele plekken",
		minZoom: 0,
		maxZoom: 19,
		opacity: 1.0
	});
	
	gbrww_hv_map.overlayMapTypes.push(koelevlakken);
	*/
	
	var action = {
	    'action': 'ajax_hv_get_locations'
	};
				
	jQuery.post(gbrww_ajaxURL, action, function(response) {
		
		gbrww_hv_locations = jQuery.parseJSON(response);
				
		if (window.location.hash && window.location.hash.substring(1) === 'refresh-locations') {
			
			gbrww_hv_refresh_locations('koele-buitenplek');
			gbrww_hv_refresh_locations('koele-binnenplek');
			gbrww_hv_refresh_locations('waterpunt');
		}
		
	    gbrww_infobox = new InfoBox({
			disableAutoPan: false,
			maxWidth: 240,
			boxClass: "gbrww__map__infobox",
			closeBoxURL: "",
			infoBoxClearance: new google.maps.Size(20, 140),
			isHidden: false,
			pane: "floatPane",
			pixelOffset: new google.maps.Size(-125, 0),
			enableEventPropagation: false
	    });
	    				
		gbrww_infobox.addListener('domready', function() {
	
			jQuery('.gbrww__map__infobox__rating__star').on('click', gbrww_hv_update_rating);
		});
	
	    google.maps.event.addListener(gbrww_hv_map, 'click', function() {
		    if (gbrww_infobox){
		       gbrww_infobox.close();
		    }
		});
	
		var marker, iconCode;
	
		if (gbrww_hv_locations !== undefined && gbrww_hv_locations.length !== 0) {
			    				
		    for (var i = 0; i < gbrww_hv_locations.length; i++) {
			    
				marker = new google.maps.Marker({
					position: {lat: parseFloat(gbrww_hv_locations[i]['pos']['lat']), lng: parseFloat(gbrww_hv_locations[i]['pos']['lng'])},
					map: gbrww_hv_map,
					clickable: true
				});
				
				if (gbrww_hv_locations[i]['type']['value'] === 'koele-buitenplek') {
					iconCode = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAACXBIWXMAACxKAAAsSgF3enRNAAADuUlEQVR4nO2cMW/TQBSAvyICgqGt1AWk4EYdYOgAyGuRypQN+g8aBhYW+hO6sZaRrfwC0tFTg5gjFbEhga5RRZGgUlMBFSpVGXyRSkgcN/a9e478SVajxLn3+unZZz9fO3V2dkbJ+FzynUDRKQVmpBSYkVJgRkqBGSkFZqQUmJHLvhMYRBiZe8Bs39uH7Xptx0c+SUxpuJAOIzMLrNjt8Yjdt4Am0GzXa4eucxuFV4FW3JrdZi749S6wAWz4FOlNYBiZZWATmM841C7QaNdrrYzjjIWXSSSMzDqwTXZ52DG2w8is5TDWhRGvwDAym8Cqo+Fft+u1hqOxByJagbZKXMkDWJWuRLEKtOe8bZFg8FDqnChZgZuTGEtEYBiZBvlMGGmZtzGdI1WB60JxxGM6FxhGZgXZ6usxb2M7RaIClwVieItdCsyIhMC7AjG8xS77gRmZeIG24+OMiRfomokX6LpXKCGwKxDDW2wJgT6fYziPLSGwJRBjGE3XASQEOv8lfMZ2LtA+inzrOs4Attr1mnEdZJK7MRsSQUQE2u6wZBW+nMSOdAOZS5r3CFa8mEB7PnLdn+sSPyMWe9AueidiD6snuKnELrAsvX7Gy8oEu3ioSX6d6vfAisSs24/PpR2zxOeq5xmG6RKvjVnPI6dx8L46K4xMjXhxUYP0C4x6C4s2fVTdebwLPE8YmUNGS+y26zWnPb6LoK2dlWYCULXIUpvAwlEKzIg2ga2c9hFDm8DCUQrMiDaB5SyckTRNAO9/2nAebQILhzaBJsU+qipQ1a0cQBiZxITa9dqUVC5p0FaBhUOjQJ8rGS6MRoFJlyk+Ho8molFgodAoUNUsOwqNApMOYVV3IaBTYBLqqrNoAtWhUWAr4TMjlENqNApMwvhOoJ+iCVSHRoFJM626SURdMwGGNxS0NRJAZwUWCq0Cd30nkBatAs2A99Q1EkCvwMKgVaC62XYYWgUOupRR10gAvQIHobIqiyRQJSr+AWMQVK8D14AKMDP97MWnq/cf/LPP6RdzJQiW7gDf7VvHnc7eL9lM/8eLwCCo3gDmiFejzvV/frpvbtEn8PfOux/Abbv1xgE4IH4QddDp7H11l/VgRAUGQXWBWEAlx2Hn7LYQBNUT4GOns/c5x/ETETsHBkF1Glgkhbyfb159GDNMBVi0sUSQrMA/wAkpq+/b06VHY8Y5sbFEEO3GBEG1QnwI3ySeNPLkGNgnPoRPch57KN7aWfYwmwF6Pyv2dRqOiCuta193O529Ixd5juIvgsf+PIIeCYIAAAAASUVORK5CYII=';
				} else if (gbrww_hv_locations[i]['type']['value'] === 'waterpunt') {
					iconCode = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAACXBIWXMAACE3AAAhNwEzWJ96AAACqUlEQVRoge2aT27TUBCHvwBBahctUrxgYT3KqstWldh043ACygWicIMegSP0CMEnoGKJkOILoHbZVV0rCyQSCYPUSi0oLPwiouA4EM+8VI6/TSL/+Xl+mcl7fmM3xuMx68SDVQfgmtpw1akNV51Hri407ARPgH2gPbOrD5x5YfTNRRwN7Wlp2AnawDHwasGhp8CJF0Z9zXjUDNuM9lhsdJZToKuVcRXDw06wT1aq20tKXAFHXhidiQVlETcsYHZCCrSlTYuO0lNlXNYsVqNnNcWQnpaOgT1BvT2rKYZYSdtMxMhkd5oU2JEaxCQz3EXeLFazKyUmafhIUEtNW9JwIKilpr1299K14apTGy7BuaCWmrak4b6glpq2pOGeoNYsJ1JCYobtqiaS0pvinRdGsZSY9KDVJbv3lSJFePEgathmQjLAtnTnQ3xa8sKoB7wpKZMCrzU6HirzsDX9kqxV87+ck2X2vWhQFtWupV0jd8nK/NmCw6+At/bHUkO9TTth2An6zF/1RF4YtV3E4fLWMl5ynyi14arj0nDRfOrkuRK4NVw0p4rPt/OoS1oRZ2VbhLN5GGDYCXIv5oVRw1UMdUlXHdeG8xoEmr2wv7gPGXY6mN0Hw05xbTjvBiN2GYBrw3nlG7sMoC5pZeKcbZUetOKcbc4WDlCXtDorX0A4NTynz7xeJe3qLdoJKzfsmlUYnl5ALPNkohRqL4gb428CLWDyCdD8eLjTaj1+CMDo9tePA+MfTp02Aq6BUZIMrjXiEjdsjP8U2AW2/vGUVt53Y/zvwEWSDL4IhqeS4RdFOz+nN58OtjcA+Hr787Lg0C2r9UEuNIWeljH+LvAcaJaUugMuk2RwUT6qP6g08Yzxm2Tl2SJ7OXQT2Fhw2g3Z/zcl+y+PkmRwJx3bbxP8v3Hez+xoAAAAAElFTkSuQmCC';
				} else {
					iconCode = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFEAAABQCAYAAABh05mTAAAACXBIWXMAACxKAAAsSgF3enRNAAADoUlEQVR4nO2cQU7bQBRAXyiI0EWhogsWqZG6aBcsmk3XcARu4NygNAeoOIELNyA3oDdI1t2Aqm6rxM2CRWkLFdQVldJFBgQhiU0y8+cb/KTIIJv5X08zHnv+hFKv16NgOmZ8J3AfKCRaoJBogUKiBQqJFigkWqCQaIFZ3wkMo1xvLAHVIacOkij8JZ1PGiUtD9vleqMK1IBNYHXMpR1gH9hLovBAILVUvEss1xsbwDawPsGft4DtJAqbFlO6M94kmiG7Dby10NwufZlehroXiWbo7gGvLTZ7CGwmUdi22GYmxCUagU1g0UHzJ8CG9L1SVKIZwgeMnzimpQNUJYe29HPiPm4FYtrfdxzjBmISy/VGjclm4ElYN/FEkOyJ24KxROOJSDS9wvUwHmRVqjdK9cQtoThe4jqfnc2M/NNpkPE8dT1TS/TEDYEYXuNLSBy2GiOJ8/jFeqIFHoLEJdcBHoJE5zwEic7foSUk+l7OvxcSfS/hO48vshRWrjd81SBOkii8NxPLR6E4g4gsiUlJ3BGK4yWuiERTjWtJxLpGS6pMcF/XE0/o17BFEJNoeuOuULgtyaqfj2pfE7dlgkYShTWH7d/CxxvLJu7uj+ICwe8OiB3s7H645F0ShV6eArzuxTH7cPaYrv7Son8P9PZm5H1DE1wVsmrc7V7ZAnaSKBStMQ9DhcRLyvXGFvAhw6Xehu4wtC2FZR2Svhc1bqBNYi4pJFpA1T0Rsi2bJVFYksglK0VPtEAh0QIaJR5OeV4cjRLTCku+C1+30Cgxd2iU2E45X/TEDLRTzqt6WwGdEnOHRonqhmsaGiWmDdemRBJ3QaPE3KFRYjGcpyXDMn8xO0+Lxm/e506iRrRKHFWX7ohmkRGtEkfR9p3AMPImUSVaJY6agdVNKqBX4ihZ6h5vQK/EXKFVYtt3AnchbxKbgjlkRqvEXKFVospZeBQqJY5ZhChm52nRuPgAOZOoFTX/hDIIKsvmx0VgrrT5/lNvdv7N1QW93u8gqLwCzs2HOO4eiyc6BC8Sg6DyGFgBlulLWxi8pvTvYqE3O3/1+8zfsw7wcqAdgD/0v/xzDBzFcffcWeIjEJVo5FXpy7PFgvmsAGtBUDkGDiRlSt8T18go8NHZj88Txlg2ccQQ3eQZBJU14IVAqK9x3P0iEAfwsFM2CCorwHP6w882R8C3OO4eOWh7JN62GwdBZQ54Ajwzx7lrxzQugNNrx+/AaRx3L9xkO57/V24KQwpK79AAAAAASUVORK5CYII=';
				}
	
				marker.setIcon({
					url: iconCode,
					scaledSize: new google.maps.Size(30, 30)
				});
	
				gbrww_hv_gbrww_st_markers.push(marker);
	
				google.maps.event.addListener(marker, 'click', (function(marker, i) {
	
					return function() {
						gbrww_hv_highlight_location(marker, gbrww_hv_locations[i], gbrww_infobox);
					};
				})(marker, i));
	
	                  
		    }
			
	        var ClusterStyles = [
	          [
	            {
	              width: 26,
	              height: 26,
	              className: 'custom-clustericon-1'
	            },
	            {
	              width: 34,
	              height: 34,
	              className: 'custom-clustericon-2'
	            },
	            {
	              width: 40,
	              height: 40,
	              className: 'custom-clustericon-3'
	            }
	          ]
	        ];
	        
			new MarkerClusterer(gbrww_hv_map, gbrww_hv_gbrww_st_markers, {
				ignoreHiddengbrww_st_markers: true,
				styles: ClusterStyles[0],
				maxZoom: 15,
				imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m',
				clusterClass: 'custom-clustericon'
	        });
			
			gbrww_hv_find_nearest(51.921952, 4.463931);
					
		}
		
	});
	
	jQuery('#gbrww-hv-search').submit(function(e) {
		
		e.preventDefault();
		
		jQuery('.gbrww-hv__error').hide();
		
		var postcode = gbrww_sanitize_postcode(jQuery('#gbrww-hv-search-postcode').val());
	    var houseno = gbrww_sanitize_houseno(jQuery('#gbrww-hv-search-houseno').val());
	    		
		if (postcode && houseno) {
	
			if (gbrww_is_postcode_buiten_gemeente(postcode)) {
				
				jQuery('#gbrww-hv-search-error').html('Deze tool is alleen beschikbaar binnen de gemeente Rotterdam').show();
					
			} else {
					
			    var apiURL = 'https://api.pdok.nl/bzk/locatieserver/search/v3_1/suggest?q=' + houseno + ',%20' + postcode;
			
			    jQuery.ajax({
			        url: apiURL,
			        type: 'GET'
			    })
			    .done(function(data) {
			
				    if (data.response.numFound !== 0) {
			
					    var adresCode = data.response.docs[0].id;
						
					    var apiURL = 'https://api.pdok.nl/bzk/locatieserver/search/v3_1/lookup?id=' + adresCode + '&fq=type:adres';
					
					    jQuery.ajax({
					        url: apiURL,
					        type: 'GET'
					    })
					    .done(function(data) {
					
						    if (data.response.numFound !== 0) {
					
							    var buildingData = data.response.docs[0];
					
							    var coords = buildingData.centroide_ll;
							    coords = coords.replace('POINT(', '');
							    coords = coords.replace(')', '');
							    coords = coords.split(' ');
							    
							    gbrww_hv_find_nearest(parseFloat(coords[1]), parseFloat(coords[0]));
							    				
							}
					    });
			
			        } else {
						jQuery('#gbrww-hv-search-error').html('Er zijn geen gegevens gevonden voor deze combinatie van postcode en huisnummer').show();
			        }
			    })
			    .fail(function() {
					jQuery('#gbrww-hv-search-error').html('Er zijn geen gegevens gevonden voor deze combinatie van postcode en huisnummer').show();
			    });
			
			}
	
		} else {
	
			if (!postcode) {
				
				jQuery('#gbrww-hv-search-error-postcode').html('Deze postcode is niet geldig. Vul de vier cijfers en twee letters van je postcode in.').show();
			}
	
			if (!houseno) {
				jQuery('#gbrww-hv-search-error-houseno').html('Dit huisnummer is niet geldig. Vul je huisnummer met eventuele toevoegingen in.').show();
			}
		}
		
	});
	
	jQuery('.gbrww-hv__nearest').on('click', '.gbrww-hv__nearest__list__link', function(e) {
		
		e.preventDefault();
		
		var locationIndex = parseInt(jQuery(this).attr('data-locationindex'));
		gbrww_hv_highlight_location(gbrww_hv_gbrww_st_markers[locationIndex], gbrww_hv_locations[locationIndex], gbrww_infobox);
		
	});
	
	jQuery('#gbrww-hv-current-location').click(function(e) {
		
		e.preventDefault();
		
		jQuery('.gbrww-hv__error').hide();
		
		if (navigator.geolocation) {
			
			navigator.geolocation.getCurrentPosition(function(pos) {
								
				if (pos.coords.latitude > 51.829651 && pos.coords.latitude < 51.985184 && 
					pos.coords.longitude > 4.226180 && pos.coords.longitude < 4.641082) {
				
					gbrww_hv_find_nearest(pos.coords.latitude, pos.coords.longitude);
					
				} else {
					
					jQuery('#gbrww-hv-current-location-error').html('Deze tool is alleen beschikbaar binnen de gemeente Rotterdam').show();
				
					
				}
			
			}, function() {
				
				jQuery('#gbrww-hv-current-location-error').html('We kunnen je locatie helaas niet vinden. Probeer het later nog eens!').show();
				
			});
		} else {
			
			jQuery('#gbrww-hv-current-location-error').html('Je browser ondersteunt helaas geen geolocatie.').show();
		
		}
	})
}

function gbrww_hv_get_current_rating(pos) {
	
	var foundRating = false;
	
	jQuery.each(gbrww_hv_locations, function(i, location) { 
				
	    if (location.pos.lat === pos.lat && location.pos.lng === pos.lng && location.rating !== undefined) {
	        foundRating = [location.rating, location.ratings];
	    }
	});
	
	return foundRating;
}

function gbrww_hv_refresh_locations(type) {
	
	var apiURL;
	
	if (type === 'koele-buitenplek') {
		
		// locaties uit WFS halen
		// bij elke nieuwe locatie checken of er al een rating was voor deze coordinaten
	
	    //apiURL = 'https://dservices.arcgis.com/zP1tGdLpGvt2qNJ6/arcgis/services/Koeleplekken_points_MetGroennamenGrotePParken_StrNaam/WFSServer?SERVICE=WFS&VERSION=1.1.0&REQUEST=getfeature&typenames=Koeleplekken_points_GroennamenGrotePParken_StrNaamOverigeParken';
		
		apiURL = 'https://dservices.arcgis.com/zP1tGdLpGvt2qNJ6/arcgis/services/Bestaande_koeleplekken_punt/WFSServer?SERVICE=WFS&VERSION=1.1.0&REQUEST=getfeature&typenames=Bestaande_koeleplekken_naam';
	    	    
	    jQuery.ajax({
	        url: apiURL,
	        type: 'GET'
	    })
	    .done(function(data) {
		    
		    var featureElem, coords, locTitle;
			var foundLocations = [];
			var foundLocation;
			var foundPos;
			var currentRating;
			
			var features = jQuery('gml\\:featureMember, featureMember', data);
			
		    console.log('koele-buitenplek features ' + features.length);
		    
			features.each(function(i, feature) {
				
				featureElem = jQuery(feature);
				
			    coords = featureElem.find('gml\\:pos').text().split(' ');
			    
			    foundPos = gbrww_convert_rd_to_latlng(parseFloat(coords[0]), parseFloat(coords[1]));
			    
			    //locTitle = featureElem.find('Koeleplekken_points_MetGroennamenGrotePParken_StrNaam\\:STRNAAM').text();
			    locTitle = featureElem.find('Bestaande_koeleplekken_punt\\:Naam').text();
			    
			    foundLocation = {
					title: locTitle,
					pos: foundPos
				};
				
				currentRating = gbrww_hv_get_current_rating(foundPos);
								
				if (currentRating !== false) {
					foundLocation.rating = currentRating[0];
					foundLocation.ratings = currentRating[1];
				}
				
				foundLocations.push(foundLocation);	
			
			});
			
			// handmatige locaties uit Groenblauwe site halen
			// bij elke nieuwe locatie checken of er al een rating was voor deze coordinaten
				
		    jQuery.ajax({
		        url: 'https://rotterdamsweerwoord.groenblauwemaatregelen.nl/wp-json/rww/v1/hv-locations',
		        type: 'GET'
		    })
		    .done(function(manualLocations) {
			    
				jQuery.each(manualLocations, function (i, location) {
					
					if (location.type.value === type) {
					
						foundLocations.push({
							title: location.title,
							pos: {
								lat: location.coords.lat,
								lng: location.coords.lng
							}
						});
					}
				});

				var action = {
				    'action': 'ajax_hv_set_option',
				    'type': type,
				    'locations': JSON.stringify(foundLocations)
				};
							
				jQuery.post(gbrww_ajaxURL, action, function(response) {
					//console.log(response);
				});
							
		    })
		    .fail(function() {
		
		        console.log('API error rotterdamsweerwoord.groenblauwemaatregelen.nl/wp-json/rww/v1/hv-locations');
		    });	
						
	    })
	    .fail(function() {
	
	        console.log('API error Koeleplekken_points_MetGroennamenGrotePParken_StrNaam');
	    });	
	    
	} else if (type === 'waterpunt') {
		
		// locaties uit WFS halen
		// bij elke nieuwe locatie checken of er al een rating was voor deze coordinaten
	
	    //apiURL = 'https://dservices.arcgis.com/zP1tGdLpGvt2qNJ6/arcgis/services/Drinkwaterlocaties_Rotterdam_en_Omstreken/WFSServer?SERVICE=WFS&VERSION=1.1.0&REQUEST=getfeature&typenames=Drinkwaterlocaties_Rotterdam_en_Omstreken';
		
		apiURL = 'https://dservices.arcgis.com/zP1tGdLpGvt2qNJ6/arcgis/services/Drinkwaterlocaties_Rotterdam_en_Omstreken/WFSServer?SERVICE=WFS&VERSION=1.1.0&REQUEST=getfeature&typenames=Drinkwaterlocaties_Rotterdam_en_Omstreken';
	    
	    jQuery.ajax({
	        url: apiURL,
	        type: 'GET'
	    })
	    .done(function(data) {
		    
		    var featureElem, coords;
			var foundLocations = [];
			var foundLocation;
			var foundPos;
			var currentRating;
			
			var features = jQuery('gml\\:featureMember, featureMember', data);
		    
		    console.log('waterpunt features ' + features.length);
		    
			features.each(function(i, feature) {
				
				featureElem = jQuery(feature);
				
			    coords = featureElem.find('gml\\:pos').text().split(' ');
			    
			    foundPos = gbrww_convert_rd_to_latlng(parseFloat(coords[0]), parseFloat(coords[1]));
			    
			    foundLocation = {
					title: featureElem.find('Drinkwaterlocaties_Rotterdam_en_Omstreken\\:Comment').text(),
					pos: foundPos
				};
				
				currentRating = gbrww_hv_get_current_rating(foundPos);
								
				if (currentRating !== false) {
					foundLocation.rating = currentRating[0];
					foundLocation.ratings = currentRating[1];
				}
				
				foundLocations.push(foundLocation);	
			
			});
			
			// handmatige locaties uit Groenblauwe site halen
			// bij elke nieuwe locatie checken of er al een rating was voor deze coordinaten
				
		    jQuery.ajax({
		        url: 'https://rotterdamsweerwoord.groenblauwemaatregelen.nl/wp-json/rww/v1/hv-locations',
		        type: 'GET'
		    })
		    .done(function(manualLocations) {
			    
				jQuery.each(manualLocations, function (i, location) {
					
					if (location.type.value === type) {
					
						foundLocations.push({
							title: location.title,
							pos: {
								lat: location.coords.lat,
								lng: location.coords.lng
							}
						});
					}
				});

				var action = {
				    'action': 'ajax_hv_set_option',
				    'type': type,
				    'locations': JSON.stringify(foundLocations)
				};
							
				jQuery.post(gbrww_ajaxURL, action, function(response) {
					//console.log(response);
				});
							
		    })
		    .fail(function() {
		
		        console.log('API error rotterdamsweerwoord.groenblauwemaatregelen.nl/wp-json/rww/v1/hv-locations');
		    });
						
	    })
	    .fail(function() {
	
	        console.log('API error Koeleplekken_points_MetGroennamenGrotePParken_StrNaam');
	    });	
		
	} else if (type === 'koele-binnenplek') {
		
		// locaties uit WFS halen
		// bij elke nieuwe locatie checken of er al een rating was voor deze coordinaten
	
	    //apiURL = 'https://dservices.arcgis.com/zP1tGdLpGvt2qNJ6/arcgis/services/coolcenters_bijgewerkt20052021/WFSServer?SERVICE=WFS&VERSION=1.1.0&REQUEST=getfeature&typenames=coolcenters_bijgewerkt20052021';
	    
	   //apiURL = 'https://dservices.arcgis.com/zP1tGdLpGvt2qNJ6/arcgis/services/coolcenters_RD_Output/WFSServer?SERVICE=WFS&VERSION=1.1.0&REQUEST=getfeature&typenames=coolcenters_RD_Output';
	   
	   apiURL = 'https://dservices.arcgis.com/zP1tGdLpGvt2qNJ6/arcgis/services/Koele_binnenplekken/WFSServer?SERVICE=WFS&VERSION=1.1.0&REQUEST=getfeature&typenames=Koele_binnen_plekken';
	    	    
	    jQuery.ajax({
	        url: apiURL,
	        type: 'GET'
	    })
	    .done(function(data) {
		    
		    var featureElem, coords;
			var foundLocations = [];
			var foundLocation;
			var foundPos;
			var currentRating;
			var fee, locTitle, locCat;
			
			var features = jQuery('gml\\:featureMember, featureMember', data);
		    
		    console.log('koele-binnenplek features ' + features.length);
		    
			features.each(function(i, feature) {
				
				featureElem = jQuery(feature);
				
			    coords = featureElem.find('gml\\:pos').text().split(' ');
			    
			    foundPos = gbrww_convert_rd_to_latlng(parseFloat(coords[0]), parseFloat(coords[1]));
			    
			    fee = featureElem.find('Koele_binnenplekken\\:access').text().trim().toLowerCase() == 'free' ? 'gratis' : 'niet gratis';
			    
			    locTitle = featureElem.find('Koele_binnenplekken\\:name').text();
			    
			    locCat = featureElem.find('Koele_binnenplekken\\:type').text();
				 
			    foundLocation = {
					title: locTitle,
					cat: locCat,
					cost: fee,
					pos: foundPos
				};

				currentRating = gbrww_hv_get_current_rating(foundPos);
								
				if (currentRating !== false) {
					foundLocation.rating = currentRating[0];
					foundLocation.ratings = currentRating[1];
				}
				
				foundLocations.push(foundLocation);   
			
			});
			
			// handmatige locaties uit Groenblauwe site halen
			// bij elke nieuwe locatie checken of er al een rating was voor deze coordinaten
				
		    jQuery.ajax({
		        url: 'https://rotterdamsweerwoord.groenblauwemaatregelen.nl/wp-json/rww/v1/hv-locations',
		        type: 'GET'
		    })
		    .done(function(manualLocations) {
			    
				jQuery.each(manualLocations, function (i, location) {
					
					if (location.type.value === type) {
					
						foundLocations.push({
							title: location.title,
							pos: {
								lat: location.coords.lat,
								lng: location.coords.lng
							}
						});
					}
				});

				var action = {
				    'action': 'ajax_hv_set_option',
				    'type': type,
				    'locations': JSON.stringify(foundLocations)
				};
							
				jQuery.post(gbrww_ajaxURL, action, function(response) {
					//console.log(response);
				});
							
		    })
		    .fail(function() {
		
		        console.log('API error rotterdamsweerwoord.groenblauwemaatregelen.nl/wp-json/rww/v1/hv-locations');
		    });
						
	    })
	    .fail(function() {
	
	        console.log('API error Koeleplekken_points_MetGroennamenGrotePParken_StrNaam');
	    });	
		
	}
}

function gbrww_slide_complete(args) {

	jQuery('.next, .prev').removeClass('unselectable');

    if(args.currentSlideNumber == 1) {

        jQuery('.prev').addClass('unselectable');

    } else if(args.currentSliderOffset == args.data.sliderMax) {

        jQuery('.next').addClass('unselectable');

    }

}

function gbrww_hv_display_tips(tips) {
	
	var tipsElem = jQuery('#gbrww-hv-tips');
		
	jQuery.each(tips, function (i, tip) {
		
		tipsElem.append('<div class="gbrww-hv__tips__slide"><div class="gbrww-hv__tips__slider__link"><div class="gbrww-hv__tips__slider__img" style="background-image: url(' + tip.img + ');"></div><div class="gbrww-hv__tips__slider__info"><h3>' + tip.title + '</h3><div class="gbrww-hv__tips__slider__more">' + tip.desc + '</div></div></div></div>');
		
	});
	
	var sliderElem = jQuery('#gbrww-hv-slider');

	if (sliderElem.length) {

		var parentElem = sliderElem.parent();

		sliderElem.iosSlider({
			snapToChildren: true,
			desktopClickDrag: true,
			keyboardControls: true,
			onSlideComplete: gbrww_slide_complete,
			navPrevSelector: parentElem.find('.gbrww-hv__tips__slider__prev'),
			navNextSelector: parentElem.find('.gbrww-hv__tips__slider__next'),
		    infiniteSlider: true,
		    onSliderResize: gbrww_window_resize
		});
		
		gbrww_window_resize();

	}
}

function gbrww_hv_load_tips() {
	
	var action;
	
	if (window.location.hash && window.location.hash.substring(1) === 'refresh-tips') {
		
		console.log('ajax_hv_load_tips');
					
		action = {
		    'action': 'ajax_hv_load_tips'
		};
					
		jQuery.post(gbrww_ajaxURL, action, function(response) {
			
			console.log(response);
			
			gbrww_hv_display_tips(jQuery.parseJSON(response));
			
		});
		
	} else {
		
		//console.log('ajax_hv_get_option');
		
		action = {
		    'action': 'ajax_hv_get_option',
		    'type': 'tips'
		};
		
		jQuery.post(gbrww_ajaxURL, action, function(response) {
			
			gbrww_hv_display_tips(jQuery.parseJSON(response));
			
		});
		
	}
	
}

function gbrww_st_load_actions () {	
	
	var takenActions = [];
	
	var action = {
	    'action': 'ajax_st_load_actions'
	};
				
	jQuery.post(gbrww_ajaxURL, action, function(response) {
		
		var actionsHTML, themeSlug, img;
		
		console.log('response');
		console.log(typeof response);
		console.log(response);
		
		gbrww_st_allActions = jQuery.parseJSON(response);
				
		jQuery.each(gbrww_themes, function (i, theme) {
			
			actionsHTML = '';
			themeSlug = theme['value'];
					
			jQuery.each(gbrww_st_allActions, function (j, action) {
	
				if (action['tabs'] && jQuery.inArray(themeSlug, action['tabs']) !== -1) {
	
					if (action['img'] !== '') {
						img = '<img class="gbrww-st__measure__img" src="' + action['img'][0] + '" alt="' + action['img_caption'] + '">';
					} else {
						img = '';
					}
				
					actionsHTML += '<div class="gbrww-st__measure"' + action['filterattrs'].join(' ') + '>' + img + '<div class="gbrww-st__measure__info"><h4 class="gbrww-st__measure__info__title">' + action['title'] + '</h4><div class="gbrww-st__measure__info__desc">' + action['excerpt'] + ' <a href="#" class="gbrww-st__measure__info__more" data-actionid="' + action['id'] + '">Lees meer</a></div><label class="gbrww-st__checkbox">Selecteer deze maatregel<input id="gbrww-st-checkbox-' + action['id'] + '" class="gbrww-st__checkbox__input gbrww-st__checkbox__input--' + action['id'] + '" type="checkbox" data-actionid="' + action['id'] + '"' + (jQuery.inArray(action['id'], takenActions) !== -1 ? ' checked' : '') + '><span class="gbrww-st__checkbox__mark"></span></label></div></div>';
	
				}
			});
		
			jQuery('#gbrww-st-actions-'+themeSlug).html(actionsHTML);
			
			if (typeof(addressInfo) !== 'undefined') {
		
				if (addressInfo.postcode !== '' && addressInfo.houseno !== '') {
		
					jQuery('#gbrww-st-share, #gbrww-st-map').show();
					
					gbrww_st_map.resize();
		
					gbrww_st_addressPostcode = addressInfo.postcode;
					gbrww_st_addressHouseno = addressInfo.houseno;
		
					gbrww_st_search_postcodehouseno(gbrww_st_addressPostcode, gbrww_st_addressHouseno);
				}
			}
		});
		
	});
}
function gbrww_st_gbrww_st_convert_numeral_label_to_char(num) {

	var character;

	if (num === null || num > 6 || num === '') {
		//character = '?';
		character = 'g';
	} else {
		character = String('abcdefg').charAt(num);
	}

	return character;
}

function gbrww_st_hide_building_outline() {

	if (gbrww_st_map.getLayer('currentBuildingFill') !== undefined) {
		gbrww_st_map.setLayoutProperty('currentBuildingFill', 'visibility', 'none');
		gbrww_st_map.setLayoutProperty('currentBuildingStroke', 'visibility', 'none');
	}
}

function gbrww_st_clear_building_info() {
	
	//console.log('gbrww_st_clear_building_info');

	jQuery('#gbrww-st-share, #gbrww-st-tool, #delfland-share').hide();
	//jQuery('#gbrww_st_super').hide();
	jQuery('#gbrww-st-tool-address').html('');
	jQuery('#gbrww-st-tool-result-confirm, #gbrww-st-search').show();
	jQuery('#gbrww-st-searchform-postcode, #gbrww-st-searchform-houseno').val('');
	jQuery('#gbrww-st-saveform-name').val('');	
	
	if (isSet === 0) {
		
		var newLocations = [];
		
		for (var i; i<locations.length; i++) {
			if (locations[i].addressID !== addressID) {
				newLocations.push(locations[i]);
			}
		}
		
		locations = newLocations;
		
	}

	jQuery('.gbrww-st__checkbox__input').prop('checked', false);
    takenActions = [];
    gbrww_st_currentBuildingOutline = '';
	photoID = '';
	isSet = 0;
	addressID = '';
	gbrww_st_buildingID = '';
	gbrww_st_currentBuildingCenter = '';
	gbrww_st_addressPostcode = '';
	gbrww_st_addressHouseno = '';
	addressInfo = [];

	gbrww_st_remove_uploaded_photo();
	gbrww_st_calculate_label();
    gbrww_st_set_address_cookie('');
	gbrww_st_hide_building_outline();
}

function gbrww_st_remove_uploaded_photo(e) {

	if (e !== undefined) {
		e.preventDefault();	
	}

	photoID = 0;

	jQuery('#gbrww-st-saveform-upload').show();
	jQuery('#gbrww-st-saveform-photo').hide();

	var markerIndex = gbrww_st_gbrww_st_get_location_index_by_addressid(addressID);

	if (markerIndex >= 0 && gbrww_st_markers[markerIndex] != undefined) {
		
		jQuery('.mapboxgl-popup').find('img').remove();

	}
	
	gbrww_st_set_building_label_in_db();

	gbrww_st_resize_score_map();

}

function gbrww_st_is_postcode_buiten_gemeente(postcode) {
	
	var buitenGemeente;
	    
	var vierCijfers = parseInt(postcode.substr(0, 4));

    var min = 3000;
    var max = 3199;

	buitenGemeente = vierCijfers < min || vierCijfers > max ? true : false;	

	return buitenGemeente;
}

function gbrww_st_set_searchform_error_message(where, message) {

	var elem = where === '' ? '' : '-' + where;

	jQuery('#gbrww-st-searchform-error' + elem).css('display', 'block').html(message);

}

function gbrww_st_generate_tooltip(content) {

	if (content !== '') {

		return '<a class="tooltip-trigger"><span class="tooltip-trigger__handle"></span><span class="tooltip-trigger__content">' + content + '</span></a>';
	}
}

function gbrww_st_filter_measures(filterValues) {
	
	var thisThemeElem, thisMeasureElem;
	var showMeasure, attr, attrValues;

	jQuery('.gbrww-st__theme').each(function () {

		thisThemeElem = jQuery(this);

		thisThemeElem.find('.gbrww-st__measure').each(function () {

			thisMeasureElem = jQuery(this);
			showMeasure = false;

			jQuery.each(filterValues, function(key, value) {

				attr = thisMeasureElem.attr('data-'+key);

				if (attr !== undefined) {

					attrValues = attr.split('-');

					if (key === 'icoonsoorten') {

						for (var i=0; i<value.length; i++) {

							if (jQuery.inArray(value[i].replace(/\s/g, ''), attrValues) !== -1) {
								showMeasure = true;
							}
						}

					} else {

						if (jQuery.inArray(value, attrValues) !== -1) {
							showMeasure = true;
						}

					}
				}
			});

			if (showMeasure) {
				thisMeasureElem.show();
			} else {
				thisMeasureElem.hide();
			}

		});

	});
}

function gbrww_st_sanitize_postcode(postcode) {

	postcode = postcode.replace(/\s/g, '').toUpperCase();

	var valid = true;

	if (postcode.length === 6) {

		for (var i=0; i<6; i++) {

			if ( (i<4 && !postcode[i].match(/[0-9]/i)) || (i>=4 && i<6 && !postcode[i].match(/[A-Z]/i))) {
				valid = false;
			}

		}

	} else {
		valid = false;
	}

	return valid ? postcode : false;
}

function gbrww_st_resize_score_map() {

	var scoreMapElem = jQuery('#yourscore-map');

	if (scoreMapElem.length) {

		if (gbrww_browserWidth < 800) {

			scoreMapElem.height('auto');

		} else {

			var newHeight = Math.max(gbrww_browserHeight*0.5, jQuery('#yourscore-map-panel').outerHeight() + 50);
			scoreMapElem.height(newHeight);

		}

		gbrww_st_map.resize();

	}
}

function gbrww_st_sanitize_houseno(houseno) {

	houseno = houseno.toLowerCase();

	var myArray = houseno.match(/([0-9]+)[\s-\/]*([a-z]?)[\s-\/]*([0-9]*)/);
	
	if (myArray === null) {
		
		return false;
		
	} else {
		
		var sanitizedHouseno = myArray[1];
		
		if (myArray[2] !== undefined && myArray[2] !== '') {
			sanitizedHouseno += myArray[2];
		}
		
		if (myArray[3] !== undefined && myArray[3] !== '') {
			sanitizedHouseno += '-' + myArray[3];
		}
		
		return sanitizedHouseno;
		
	}
}

function gbrww_st_get_all_building_labels(adressLabel) {
	
	//console.log('gbrww_st_get_all_building_labels');
			
	if (locations !== undefined && locations.length !== 0) {
		
		var getgbrww_st_buildingLabelsAction = {
		    'action': 'ajax_st_get_all_building_labels'
		};
	
		jQuery.post(gbrww_ajaxURL, getgbrww_st_buildingLabelsAction, function(labels) {
			
	        gbrww_st_buildingLabels = [];
	        
	        if (labels) {
		        
		        gbrww_st_buildingLabels = jQuery.parseJSON(labels);
		        	                		
	    		// generate rules for opacity and fill color
	
	    		gbrww_st_buildingColorRule = ['match', ['get', 'cdk_id']];
	    		
	    		var buildingIsSet;
	
	    	    gbrww_st_buildingLabels.forEach(function(buildingLabel) {
		    	    
		    	    buildingIsSet = false;
		    	    
		    	    for (var i=0; i<locations.length; i++) {
			    	    
			    	    if (locations[i]['gbrww_st_buildingID'] !== undefined && locations[i]['gbrww_st_buildingID'] === buildingLabel.building_id) {
				    	    buildingIsSet = true;
				    	    break;
			    	    }
		    	    }
		    	    
		    	    if (buildingIsSet) {
		    	    
		    			gbrww_st_buildingColorRule.push('bag.pand.' + buildingLabel.building_id, gbrww_st_colors[buildingLabel.label]);
		    	    }
	    	    });
	
	    	    gbrww_st_buildingColorRule.push(gbrww_st_colors[gbrww_st_colors.length-1]);
	    	    
	    	    var uniqueIDs = [];
	    	    
	    	    for (var i=2; i<gbrww_st_buildingColorRule.length; i+=2) {
		    	    	    	    
		    	    if (jQuery.inArray(gbrww_st_buildingColorRule[i], uniqueIDs) !== -1) {
			    	    
			    	    gbrww_st_buildingColorRule.splice(i, 2);
			    	    
		    	    } else {
			    	    uniqueIDs.push(gbrww_st_buildingColorRule[i]);   
		    	    }
		    	    
	    	    }
	    	    	
				if (addressID !== '' && gbrww_st_buildingID !== '' && adressLabel !== false) {
							
					var foundBuildingInColorRule = false;
				
					for (var j=2; j<gbrww_st_buildingColorRule.length-1; j+=2) {
				
						if (gbrww_st_buildingColorRule[j] === 'bag.pand.' + parseInt(gbrww_st_buildingID)) {
							gbrww_st_buildingColorRule[j+1] = gbrww_st_colors[adressLabel];
							foundBuildingInColorRule = true;
						}
					}
				
					if (!foundBuildingInColorRule) {
				
						gbrww_st_buildingColorRule.splice(gbrww_st_buildingColorRule.length-1, 0, 'bag.pand.' + parseInt(gbrww_st_buildingID));
						gbrww_st_buildingColorRule.splice(gbrww_st_buildingColorRule.length-1, 0, gbrww_st_colors[adressLabel]);
						
					}
			
				}
	    	     
	        }
		});
	}
}

function gbrww_st_show_advice(data) {
	
	//console.log('gbrww_st_show_advice');

	var advice = '';
	var filterValues = {};
	var tooltipContent = '';
	var theme;
	
	jQuery.each(gbrww_themes, function(index, themeInfo) {

		theme = themeInfo['value'];

		if (theme === 'regen') {

			// ----------- REGEN -----------

			advice = '';

			if (data.water === undefined) {

				advice += '<p>We hebben geen informatie kunnen vinden over de waterlast op jouw adres.</p>';

			} else {

				var waterString;
				var water = parseInt(data.water, 10);
				
				switch(water) {
					case 0:
						waterString = 'lage';
						filterValues['wateroverlast'] = 'laag';
					break;
					case 1:
						waterString = 'lage';
						filterValues['wateroverlast'] = 'laag';
					break;
					case 2:
						waterString = 'aanzienlijke';
						filterValues['wateroverlast'] = 'midden';
					break;
					case 3:
						waterString = 'hoge';
						filterValues['wateroverlast'] = 'hoog';
					break;
				}

				advice += '<p>Je hebt een <strong>' + waterString + '</strong> kans op wateroverlast bij hevige buien.';			
									
				if (tooltipContent !== undefined && tooltipContent !== '') {
					advice += ' ' + gbrww_st_generate_tooltip(tooltipContent);
				}
				
				advice += '</p>';
				
			}

		} else if (theme === 'hitte') {

			// ----------- HITTE -----------

			advice = '';

			if (data.hitte === undefined || data.hitte === null) {

				advice = '<p>We hebben geen informatie kunnen vinden over het hitte risico op jouw adres.</p>';

			} else {

				var hitte = parseInt(data.hitte, 10);
				var hitteString = '';
				var hitteDuiding = '';

				switch(hitte) {
					case 1:
						filterValues['hittestress'] = 'veelkoeler';
						hitteString = 'geen bovengemiddeld';
						hitteDuiding = 'Omdat de zomers steeds heter worden is het toch aan te raden om maatregelen te nemen.';
					break;
					case 2:
						filterValues['hittestress'] = 'koeler';
						hitteString = 'geen bovengemiddeld';
						hitteDuiding = 'Omdat de zomers steeds heter worden is het toch aan te raden om maatregelen te nemen.';
					break;
					case 3:
						filterValues['hittestress'] = 'gelijk';
						hitteString = 'geen bovengemiddeld';
						hitteDuiding = 'Omdat de zomers steeds heter worden is het toch aan te raden om maatregelen te nemen.';
					break;
					case 4:
						filterValues['hittestress'] = 'warmer';
						hitteString = 'een redelijk hoog';
						hitteDuiding = 'Omdat de zomers steeds heter worden is het aan te raden om maatregelen te nemen.';
					break;
					case 5:
						filterValues['hittestress'] = 'veelwarmer';
						hitteString = 'een hoog';
						hitteDuiding = 'Omdat de zomers steeds heter worden is het aan te raden om maatregelen te nemen.';
					break;
					default:
						hitteString = 'geen data over het';
						hitteDuiding = 'Omdat de zomers steeds heter worden is het aan te raden om maatregelen te nemen.';
					break;
				}

				advice += '<p>Er is <strong>'+hitteString+'</strong> risico op hittestress in jouw buurt. '+hitteDuiding+' Kijk hieronder wat je kan doen!'
									
				if (tooltipContent !== undefined && tooltipContent !== '') {
					advice += ' ' + gbrww_st_generate_tooltip(tooltipContent);
				}
				
				advice += '</p>';

			}

		} else if (theme === 'biodiversiteit') {

			// ----------- BIODIVERSITEIT -----------

			advice = '';

			if (data.groen === undefined || data.groen === null) {

				advice += '<p>We hebben geen informatie kunnen vinden over de biodiversiteit op jouw adres.</p>';

			} else {

				var groenString;
				var groen = parseFloat(data.groen);

				if (groen >= 70) {

					groenString = 'zeer veel';
					filterValues['groen'] = 'zeerveel';

				} else if (groen >= 50) {

					groenString = 'veel';
					filterValues['groen'] = 'veel';

				} else if (groen >= 30) {

					groenString = 'redelijk wat';
					filterValues['groen'] = 'redelijk';

				} else {

					groenString = 'weinig';
					filterValues['groen'] = 'weinig';

				}

				advice = '<p>Je woont in een buurt met <strong>' + groenString + '</strong> groen. Jij kan een bijdrage leveren door je eigen tuin groen in te richten. Je kunt zorgen voor een betere leefruimte, voedsel en schuilplekken voor vogels, vlinders en andere dieren in jouw tuin.';
				
				// to do
				//tooltipContent = siteVars['tool']['data']['groen_disclaimer'];
									
				if (tooltipContent !== undefined && tooltipContent !== '') {
					advice += ' ' + gbrww_st_generate_tooltip(tooltipContent);
				}
				
				advice += '</p>';

			}

		} else if (theme === 'materiaal') {

			// ----------- MATERIAAL -----------
			advice = '';

		} else if (theme === 'energie') {

			// ----------- ENERGIE -----------

			advice = '';
			
		} else if (theme === 'droogte') {

			// ----------- DROOGTE -----------
			
			advice = '<p>Er komen steeds vaker lange periodes van droogte. Het is altijd goed om je tuin te vergroenen zodat het regenwater kan wegzakken in je tuin.</p>';

			if (data.droogte === undefined || data.droogte === null) {

				//advice += '<p>We hebben geen informatie kunnen vinden over verdrogingsgevoeligsheid op jouw adres.</p>';

			} else if (data.droogte === '1' || data.droogte == 1) {
				
				advice += '<p>Let wel op dat je voor droogtebestendig groen kiest, want je bevindt je in een <strong>droogtegevoelig gebied</strong>.';
									
				if (tooltipContent !== undefined && tooltipContent !== '') {
					advice += ' ' + gbrww_st_generate_tooltip(tooltipContent);
				}
				
				advice += '</p>';
				
				filterValues['verdroging'] = 'ja';

			} else {

				filterValues['verdroging'] = 'nee';
			}
			
		} else {
			
			advice = '';
		}

		var jQuerygbrww_st_theme_advice = jQuery('#gbrww-st-theme-advice-'+theme);
		if (jQuerygbrww_st_theme_advice.length) {
			jQuerygbrww_st_theme_advice.html(advice);
		}
		
		gbrww_st_window_resize();
	});

	gbrww_st_filter_measures(filterValues);
}

function gbrww_st_set_building_fill_color(color) {
		
	if (gbrww_st_map.getLayer('currentBuildingFill') !== undefined) {
		gbrww_st_map.setPaintProperty('currentBuildingFill', 'fill-color', color);
	}
}

function gbrww_st_wms_get_feature_info_url(latlng) {
	
	var bbox = (latlng[1]-0.01) + '%2C' + (latlng[0]-0.01) + '%2C' + (latlng[1]+0.01) + '%2C' + (latlng[0]+0.01);

	var params = {
		service: 'WFS',
		version: '2.0.0',
		request: 'GetFeature',
		typeNames: 'klimaat:PANDEN_WATEROVERLAST_HITTE_GROEN',
		OUTPUTFORMAT: 'JSON',
		bbox: bbox
	};
	
	var paramString = '?';
	
	$.each(params, function(key, value) {
		
		paramString += key.toUpperCase() + '=' + value + '&';
		
	});
	
	return ('https://geodata.zuid-holland.nl/geoserver/klimaat/wfs' + paramString);
}

function gbrww_st_get_lizard_data(latlng) {
	
	//console.log('gbrww_st_get_lizard_data');

	var WMSurl = gbrww_st_wms_get_feature_info_url(latlng);
				
	jQuery.ajax({
		url: WMSurl,
		success: function (rawData) {
						
			if (rawData.features !== undefined && rawData.features[0] !== undefined && rawData.features[0].properties !== undefined) {
				
				var data = rawData.features[0].properties;
				
				gbrww_st_show_advice(data);
			}
		    		
		},
		error: function (xhr, status, error) {
			console.log(error);  
		}
	});
	
}

function gbrww_st_get_coords_by_postcode_houseno(postcode, houseno) {
	
	// lizard
	// zwijndrecht, delfland
	
	//console.log('gbrww_st_get_coords_by_postcode_houseno');
		
    var apiURL = 'https://services.arcgisonline.nl/arcgis/rest/services/Geocoder_BAG/GeocodeServer/findAddressCandidates?Adres=' + houseno.replace(/-/g, '') + '&Postcode=' + postcode + '&Woonplaats=&SingleLine=&outFields=&maxLocations=&matchOutOfRange=true&langCode=&locationType=&sourceCountry=&category=&location=&distance=&searchExtent=&outSR=&magicKey=&f=pjson';
    
    jQuery.ajax({
        url: apiURL,
        type: 'GET'
    })
    .done(function(data) {

        var address = jQuery.parseJSON(data).candidates[0];
                
		var str_loading = '<p>Even geduld, de relevante informatie voor jouw adres wordt opgehaald...</p>';

		jQuery.each(gbrww_themes, function(index, theme) {

			var adviceElem = jQuery('#gbrww-st-theme-advice-'+theme['value']);

			if (adviceElem.length) {
				adviceElem.html(str_loading);
			}
		});
		
		gbrww_st_get_lizard_data([address.location.x, address.location.y]);

    })
    .fail(function(response) {
        console.log('API error gbrww_st_get_coords_by_postcode_houseno');
        console.log(response);
    });
}

function gbrww_st_set_address_cookie(addressID) {
	
	//console.log('gbrww_st_set_address_cookie');

	var setCookieAction = {
	    'action': 'ajax_st_set_address_cookie',
	    'addressid': addressID
	};

	//console.log(setCookieAction);

	jQuery.post(gbrww_ajaxURL, setCookieAction);
}

function gbrww_st_get_pand_by_id(pandID) {
	
	var apiURL = 'https://api.bag.kadaster.nl/lvbag/individuelebevragingen/v2/panden/' + pandID;

    $.ajax({
        url: apiURL,
        type: 'GET',
        headers: {
	        'accept': 'application/hal+json',
			'Accept-Crs': 'epsg:28992',
	        'X-Api-Key': 'l73e09f22ab1764d6fbd57385d62e82ae0'
        }
    })
    .done(function(data) {

	    var buildingData = data;

		var coordinates = [];
		var latLng;
    	
    	var RD = "+proj=sterea +lat_0=52.15616055555555 +lon_0=5.38763888888889 +k=0.9999079 +x_0=155000 +y_0=463000 +ellps=bessel +towgs84=565.417,50.3319,465.552,-0.398957,0.343988,-1.8774,4.0725 +units=m +no_defs";
		var WGS84 = "+proj=longlat +datum=WGS84 +no_defs";

    	$.each(data.pand.geometrie.coordinates, function(i, shape) {
	    	
	    	coordinates.push([]);
    	
	    	$.each(data.pand.geometrie.coordinates[i], function(j, coords) {
		    	
		    	latLng = proj4(RD, WGS84, [coords[0], coords[1]]);
				coordinates[i].push(latLng);
			});
			
		});
		
    	gbrww_st_currentBuildingOutline = {
	    	'type': 'Polygon',
	    	'coordinates': coordinates
    	};

		// show building outline
		if (gbrww_st_map.getLayer('currentBuildingFill') !== undefined) {
			gbrww_st_map.removeLayer('currentBuildingFill');
			gbrww_st_map.removeLayer('currentBuildingStroke');
		}

		if (gbrww_st_map.getSource('currentBuildingFill') !== undefined) {
			gbrww_st_map.removeSource('currentBuildingFill');
			gbrww_st_map.removeSource('currentBuildingStroke');
		}

		if (gbrww_st_map.getLayer('currentBuildingFill') === undefined) {
			
			var buildingColor = gbrww_st_labelNum == undefined ? '#ff0000' : gbrww_st_colors[gbrww_st_labelNum];
			
		    gbrww_st_map.addLayer({
		        'id': 'currentBuildingFill',
		        'type': 'fill',
		        'source': {
		            'type': 'geojson',
		            'data': gbrww_st_currentBuildingOutline
		        },
		        'paint': {
					'fill-color': buildingColor
		        }
		    });

		    gbrww_st_map.addLayer({
		        'id': 'currentBuildingStroke',
		        'type': 'line',
		        'source': {
		            'type': 'geojson',
		            'data': gbrww_st_currentBuildingOutline
		        },
		        'paint': {
			        'line-color': '#000000',
			        'line-width': 3
		        }
		    });

		} else {

			gbrww_st_map.setLayoutProperty('currentBuildingFill', 'visibility', 'visible');
			gbrww_st_map.setLayoutProperty('currentBuildingStroke', 'visibility', 'visible');

		}

    })
    .fail(function() {

        console.log('Geen pand gevonden');
    });
}

function gbrww_st_get_panden_by_address_id(addressID) {
		
    var gbrww_st_apiURL = 'https://api.bag.kadaster.nl/lvbag/individuelebevragingen/v2/verblijfsobjecten/' + addressID;

    $.ajax({
        url: gbrww_st_apiURL,
        type: 'GET',
        headers: {
	        'accept': 'application/hal+json',
			'Accept-Crs': 'epsg:28992',
	        'X-Api-Key': 'l73e09f22ab1764d6fbd57385d62e82ae0'
        }
    })
    .done(function(data) {

	    var gbrww_st_panden = data.verblijfsobject.maaktDeelUitVan;

    	if (gbrww_st_panden.length === 0) {
	        console.log('Geen panden gevonden');
    	} else {
		    gbrww_st_buildingID = gbrww_st_panden[0];
	    	gbrww_st_get_pand_by_id(gbrww_st_panden[0]);
    	}

    })
    .fail(function() {

        console.log('Geen verblijfsobject gevonden');
    });
}


function gbrww_st_create_marker(addressID) {
			
	var markerIndex = gbrww_st_get_location_index_by_addressid(addressID);
		
	if (markerIndex === -1 || gbrww_st_markers[markerIndex] === undefined) {

		// put new marker on the map
		
		var newLocation = {
			'title': jQuery('#gbrww-st-saveform-name').val(),
			'coords': gbrww_st_currentBuildingCenter,
			'label': parseInt(jQuery('#gbrww-st-yourlabel').attr('data-gbrww_st_labelNum')),
			'addressid': addressID,
			'photo': photoID
		};
		
		locations.push(newLocation);

		gbrww_st_add_marker(newLocation);

	}
}


function gbrww_st_get_address_info_by_id(addressID) {
	
	//console.log('gbrww_st_get_address_info_by_id');
	
	var getAddressAction = {
	    'action': 'ajax_st_get_address_info_by_id',
	    'addressid': addressID
	};
	
	jQuery.post(gbrww_ajaxURL, getAddressAction, function(data) {

		if (data === '') {

			//console.log('Geen info gevonden bij addressID '+addressID);

		} else {
			
			var addressInfo = jQuery.parseJSON(data);
			
			var takenActionsStrings = addressInfo.actions !== undefined ? addressInfo.actions.split(',') : [];
			var takenActionsInts = [];
			
			for (var i=0; i<takenActionsStrings.length; i++) {
				takenActionsInts.push(parseInt(takenActionsStrings[i]));
			}
			
			takenActions = takenActionsInts;
			
			jQuery.each(takenActions, function(index, actionID) {
				jQuery('#gbrww-st-checkbox-'+actionID).prop('checked', true);
			});
			
			isSet = addressInfo.is_set;
			photoID = parseInt(addressInfo.photoid);
						
			jQuery('#gbrww-st-saveform-name').val(addressInfo.name);
			
			gbrww_st_calculate_label();
		}

	});
}


function gbrww_st_get_address_by_code(addressCode) {
	
	//console.log('gbrww_st_get_address_by_code');
	
    var apiURL = 'https://api.pdok.nl/bzk/locatieserver/search/v3_1/lookup?id=' + addressCode + '&fq=type:adres';
    
    jQuery.ajax({
        url: apiURL,
        type: 'GET'
    })
    .done(function(data) {
	    
	    if (data.response.numFound !== 0) {

		    var buildingData = data.response.docs[0];
		    			    
	        var formattedAddress = buildingData.weergavenaam.replace(/, ([0-9]{4})([A-Z]{2})/g, ' in');
			jQuery('#gbrww-st-tool-address').html(formattedAddress);

		    var coords = buildingData.centroide_ll;
		    coords = coords.replace('POINT(', '');
		    coords = coords.replace(')', '');
		    coords = coords.split(' ');
		
		    gbrww_st_currentBuildingCenter = coords[0] + ',' + coords[1];

		    gbrww_st_map.flyTo({
			    center: [parseFloat(coords[0]), parseFloat(coords[1])],
			    zoom: 16
			});

			addressID = buildingData.adresseerbaarobject_id;

		    gbrww_st_create_marker(addressID);
		    gbrww_st_set_address_cookie(addressID);		    
		    gbrww_st_get_address_info_by_id(addressID);
        	gbrww_st_get_panden_by_address_id(addressID);

		} else {

        	gbrww_st_clear_building_info();
        	console.log('Er zijn geen gegevens gevonden voor deze combinatie van postcode en huisnummer');

        }
    })
    .fail(function() {

    	gbrww_st_clear_building_info();
    	console.log('Er zijn geen gegevens gevonden voor deze combinatie van postcode en huisnummer');

    });

}

function gbrww_st_get_address_suggestions_by_postcode_houseno(postcode, houseno) {
	
	//console.log('gbrww_st_get_address_suggestions_by_postcode_houseno');
	
    var apiURL = 'https://api.pdok.nl/bzk/locatieserver/search/v3_1/suggest?fq=type:adres&q=' + houseno + ',' + postcode;
    
    jQuery.ajax({
        url: apiURL,
        type: 'GET'
    })
    .done(function(data) {

	    if (data.response.numFound === 0) {

        	gbrww_st_clear_building_info();
			gbrww_st_set_searchform_error_message('postcode', 'Er is geen adres gevonden met deze postcode en dit huisnummer');

        } else {

		    var adresCode = data.response.docs[0].id;
		    gbrww_st_get_address_by_code(adresCode);

        }
    })
    .fail(function() {

    	gbrww_st_clear_building_info();

    });

}

function gbrww_st_get_location_index_by_addressid(addressid) {

	var index = -1;

	for (var i=0; i<locations.length; i++) {

		if (parseInt(locations[i].addressid) === parseInt(addressid)) {
			index = i;
		}
	}

	return index;
}

function gbrww_st_search_postcodehouseno(postcode, houseno) {
	
	//console.log('gbrww_st_search_postcodehouseno');

	jQuery('#gbrww-st-searchform-error').hide().html('');

	var thisPostcode = gbrww_st_sanitize_postcode(postcode);
    var thisHouseno = gbrww_st_sanitize_houseno(houseno);

	if (thisPostcode && thisHouseno) {

		if (gbrww_st_is_postcode_buiten_gemeente(thisPostcode)) {

			gbrww_st_set_searchform_error_message('Deze tool is alleen beschikbaar in de gemeente Rotterdam');

		} else {

			jQuery('#gbrww-st-search').hide();
			var toolElem = jQuery('#gbrww-st-tool');
			toolElem.show();

			jQuery('html,body').animate({scrollTop: toolElem.offset().top}, 500);

			gbrww_st_addressPostcode = thisPostcode;
			gbrww_st_addressHouseno = thisHouseno;

			gbrww_st_get_address_suggestions_by_postcode_houseno(thisPostcode, thisHouseno);
			gbrww_st_get_coords_by_postcode_houseno(thisPostcode, thisHouseno);
				
		}

	} else {

		if (!thisPostcode) {

			gbrww_st_set_searchform_error_message('postcode', 'Vul de vier cijfers en twee letters van je postcode in');
		}

		if (!thisHouseno) {
			gbrww_st_set_searchform_error_message('houseno', 'Vul hier je huisnummer met eventuele toevoegingen in');
		}
	}

}

function gbrww_st_set_building_label_in_db() {
	
	//console.log('gbrww_st_set_building_label_in_db');
	
	var yourLabelElem = jQuery('#gbrww-st-yourlabel');

	var label = parseInt(yourLabelElem.attr('data-gbrww_st_labelNum'));

	if (addressID !== undefined && gbrww_st_buildingID !== undefined && addressID !== '' && gbrww_st_buildingID !== '') {

		var setAddressLabelAction = {
		    'action': 'ajax_st_set_address_info',
		    'label': label,
		    'addressid': addressID,
		    'gbrww_st_buildingID': gbrww_st_buildingID,
		    'actions': takenActions.join(','),
		    'name': jQuery('#gbrww-st-saveform-name').val(),
		    'coords': gbrww_st_currentBuildingCenter,
		    'postcode': gbrww_st_addressPostcode,
		    'houseno': gbrww_st_addressHouseno,
		    'photoid': photoID,
		    'is_set': isSet
		};
		
		//console.log(setAddressLabelAction);
		
		jQuery.post(gbrww_ajaxURL, setAddressLabelAction, function() {
			
			if (gbrww_st_buildingColorRule !== undefined) {

				var foundBuildingInColorRule = false;

				for (var i=2; i<gbrww_st_buildingColorRule.length-1; i+=2) {

					if (gbrww_st_buildingColorRule[i] === 'bag.pand.' + parseInt(gbrww_st_buildingID)) {
						gbrww_st_buildingColorRule[i+1] = gbrww_st_colors[label];
						foundBuildingInColorRule = true;
					}
				}

				if (!foundBuildingInColorRule) {

					gbrww_st_buildingColorRule.splice(gbrww_st_buildingColorRule.length-1, 0, 'bag.pand.' + parseInt(gbrww_st_buildingID));
					gbrww_st_buildingColorRule.splice(gbrww_st_buildingColorRule.length-1, 0, gbrww_st_colors[label]);
				
				}
									
			}
		});
	}
}

function gbrww_st_update_theme_scores() {

	var listElem, listItem, theme;

	jQuery('.gbrww-st__theme__improve__scores').each(function() {

		listElem = jQuery(this);

		listElem.children('.gbrww-st__theme__improve__score').each(function() {

			listItem = jQuery(this);

			theme = listItem.attr('data-theme');

			listItem.find('.gbrww-st__theme__improve__score__bar').css('width', gbrww_st_themeScores[theme] + '%');
			listItem.find('.gbrww-st__theme__improve__score__value').html(gbrww_st_themeScores[theme]+'%');

		});
	});
}

function gbrww_st_show_congratulations(charLabel) {
	
	var congratulations;
	
	switch(charLabel) {
		case 'a': congratulations = 'Gefeliciteerd, je bent echt fantastisch bezig!'; break;
		case 'b': congratulations = 'Gefeliciteerd, je bent super goed bezig!'; break;
		case 'c': congratulations = 'Gefeliciteerd, je bent goed bezig!'; break;
		case 'd': congratulations = 'Gefeliciteerd, je bent goed bezig!'; break;
		case 'e': congratulations = 'Gefeliciteerd, je bent al goed op weg!'; break;
		case 'f': congratulations = 'Gefeliciteerd, je hebt al wat kleine stappen gezet!'; break;
		case 'g': congratulations = 'Tijd om te ontstenen en vergroenen!'; break;
		default:  congratulations = 'Gefeliciteerd, je bent goed bezig!'
	}
		
	jQuery('#tool-congratulations').html(congratulations);
}

function gbrww_st_calculate_label() {

	jQuery.each(gbrww_st_themeScores, function(key, value) {
		gbrww_st_themeScores[key] = 0;
	});
	
	for (var i=0; i<takenActions.length; i++) {

		for (var j=0; j<gbrww_st_allActions.length; j++) {

			if (takenActions[i] === gbrww_st_allActions[j].id) {

				jQuery.each(gbrww_st_themeScores, function(key, value) {
					
					gbrww_st_themeScores[key] += parseInt(gbrww_st_allActions[j][key])
				});
			} 
		}
	}
	
	var val;
	var totalVal = 0;
	var gbrww_st_tool_theme_tabs = jQuery('.gbrww-st__themes .gbrww-st__theme');
	var cur_themes = [];
	
	jQuery.each(gbrww_st_tool_theme_tabs, function(){
		cur_themes.push(jQuery(this).attr('data-theme'));
	});

	jQuery.each(gbrww_st_themeScores, function(key, value) {
		if (jQuery.inArray(key, cur_themes) > -1) {
			val = Math.min(100, value);
			gbrww_st_themeScores[key] = val;
			totalVal += val;
		}
	});

	var averageScore = totalVal / gbrww_st_tool_theme_tabs.length;
	var newLabel = Math.round(averageScore/(100/7)-0.5) * -1 + 6;
	newLabel = Math.max(0, newLabel);
	
	var charLabel = gbrww_st_gbrww_st_convert_numeral_label_to_char(newLabel);
	
	var labelElem = jQuery('#gbrww-st-yourlabel');

	labelElem.attr('data-label', charLabel);
	labelElem.attr('data-gbrww_st_labelNum', newLabel);
	labelElem.html(charLabel.toUpperCase());
	labelElem.attr('class', 'gbrww-st-label__char gbrww-st-label__char--' + charLabel);
	
	if (gbrww_st_colors[newLabel] !== undefined) {
		gbrww_st_set_building_fill_color(gbrww_st_colors[newLabel]);
	}
	
	gbrww_st_show_congratulations(charLabel);
	gbrww_st_update_marker(addressInfo.photo);
	gbrww_st_update_theme_scores();
}

function gbrww_st_remove_action(actionID) {

	// also uncheck all other checkboxes for this action
	jQuery('.gbrww-st__checkbox__input--'+actionID).prop('checked', false);

	var index = takenActions.indexOf(actionID);

	if (index > -1) {

		// action is in array
		// remove action from array

	    takenActions.splice(index, 1);
		gbrww_st_calculate_label();
	}
}

function gbrww_st_add_action(actionID) {

	// also check all other checkboxes for this action
	jQuery('.gbrww-st__checkbox__input--'+actionID).prop('checked', true);

	if (jQuery.inArray(actionID, takenActions) === -1) {

		// action is not in array yet
		// add action to array

		takenActions.push(actionID);

		// sommige acties sluiten elkaar uit
		// dus als de ene toegevoegd wordt dan moet de andere actie verwijderd worden
		// combinaties:
		// 1284 en 1286
		// 1287 en 1288

		if (actionID === 1284) {

			jQuery('.gbrww-st__checkbox__input--1286').prop('checked', false);
			gbrww_st_remove_action(1286);

		} else if (actionID === 1286) {

			jQuery('.gbrww-st__checkbox__input--1284').prop('checked', false);
			gbrww_st_remove_action(1284);

		} else if (actionID === 1287) {

			jQuery('.gbrww-st__checkbox__input--1288').prop('checked', false);
			gbrww_st_remove_action(1288);

		} else if (actionID === 1288) {

			jQuery('.gbrww-st__checkbox__input--1287').prop('checked', false);
			gbrww_st_remove_action(1287);

		}
		
		gbrww_st_calculate_label();
	}
}


function gbrww_st_scroll_to_map() {

	var mapElem = jQuery('#gbrww-st-map');
	var scrollTo = mapElem.offset().top + mapElem.outerHeight() - gbrww_browserHeight;

	jQuery('html,body').animate({scrollTop: scrollTo}, 500);

}

jQuery(window).scroll(function() {
	
	var resultBar = jQuery('#gbrww-st-tool-result');
	var toolContainer = jQuery('#gbrww-st-tool').children().first();

	if (resultBar.length && toolContainer.length) {

		var scrollPos = jQuery(document).scrollTop();
		var toolContainerPos = toolContainer.offset();

		if (scrollPos > toolContainerPos.top + toolContainer.outerHeight() - gbrww_browserHeight + resultBar.outerHeight()) {

			resultBar.removeClass('gbrww-st__result--fixed');
			$('#gbrww-st-tool-result-placeholder').remove();
			$('#gbrww-st-share').before(resultBar);

		} else {

			resultBar.addClass('gbrww-st__result--fixed');
			$('body').append(resultBar);
			if ($('#gbrww-st-tool-result-placeholder').length === 0) {
			
				$('#gbrww-st-share').before('<div id="gbrww-st-tool-result-placeholder" style="height: ' + resultBar.outerHeight() + 'px"></div>');	
			}
		}

	}

});

function gbrww_st_window_resize() {

	var maxDescHeight = 'auto';

	if (gbrww_browserWidth >= 850) {

		maxDescHeight = 0;
		var descElemHeight;

		jQuery('.gbrww-st__theme__desc').each(function() {

			descElemHeight = jQuery(this).children('div').outerHeight();

			if (descElemHeight > maxDescHeight) {
				maxDescHeight = descElemHeight;
			}
		});
	}

	jQuery('.gbrww-st__theme__desc').css('height', maxDescHeight);

	gbrww_get_browser_size()
}

function gbrww_st_gbrww_st_get_location_index_by_addressid(addressid) {

	var index = -1;

	for (var i=0; i<locations.length; i++) {

		if (parseInt(locations[i].addressid) === parseInt(addressid)) {
			index = i;
		}
	}

	return index;
}

function gbrww_st_update_marker(photoURL = false) {
		
	var markerIndex = gbrww_st_get_location_index_by_addressid(addressID);
	
	if (markerIndex !== -1 && gbrww_st_markers[markerIndex] !== undefined) {
		
		var content = '<p style="padding-right: 35px;">' + locations[markerIndex].title + '&nbsp;</p>';
		
		if (photoURL !== undefined && photoURL !== false && photoURL !== '') {
			content += '<img src="' + photoURL + '" alt="" />';
		}
		
		gbrww_st_labelNum = parseInt(jQuery('#gbrww-st-yourlabel').attr('data-gbrww_st_labelNum'));
		
	    var labelChar = gbrww_st_convert_numeral_label_to_char(gbrww_st_labelNum);
	    
	    var markerElem = jQuery(gbrww_st_markers[markerIndex].getElement());
	    
	    if (gbrww_st_colors[gbrww_st_labelNum] !== undefined) {
		 
		    markerElem.find('g > g').eq(1).attr('fill', gbrww_st_colors[gbrww_st_labelNum]);   
	    }
	    
			
		jQuery('.mapboxgl-popup').find('.gbrww-st_marker_label').attr('class', 'gbrww-st_marker_label gbrww-st_marker_label_'+labelChar).html(labelChar.toUpperCase());
		
	    content += '<p>';
	    content += '<span class="gbrww-st_marker_label gbrww-st_marker_label_'+labelChar+'">'+labelChar.toUpperCase()+'</span>';
	    content += '<span class="gbrww-st_marker_label_text">Klimaatlabel</span>';
	    content += '</p>';

		gbrww_st_markers[markerIndex].setPopup(new mapboxgl.Popup({ offset: 0 })
			.setHTML(content))

	}
}

function gbrww_st_add_marker(location) {

	var coords = [];

	if (!jQuery.isArray(location.coords)) {
		coords = location.coords.split(',');
	} else {
		coords = location.coords;
	}

    var infoboxContent = '<p style="padding-right: 35px;">' + location.title + '</p>';
		
    if (location.photo !== undefined && location.photo !== false && location.photo !== '') {
	    infoboxContent += '<img src="' + location.photo + '" alt="" />';
    }
    
	var labelChar = gbrww_st_convert_numeral_label_to_char(location.label);
    infoboxContent += '<p>';
    infoboxContent += '<span class="gbrww-st_marker_label gbrww-st_marker_label_'+labelChar+'">'+labelChar.toUpperCase()+'</span>';
    infoboxContent += '<span class="gbrww-st_marker_label_text">Klimaatlabel</span>';
    infoboxContent += '</p>';
  
	var markerColor = location.label === '-' ? 6 : location.label;
				
	var marker = new mapboxgl.Marker({color: gbrww_st_colors[markerColor]});
  
	marker.setLngLat([parseFloat(coords[0]), parseFloat(coords[1])])
		.setPopup(new mapboxgl.Popup({ offset: 0 })
			.setHTML(infoboxContent))
		.addTo(gbrww_st_map);

	gbrww_st_markers.push(marker);
}

function gbrww_st_open_popup_emmerproef(e) {
	
	e.preventDefault();
	
	jQuery('body').addClass('no-scroll');
	
	jQuery('#gbrww-st-action-overlay').fadeIn('fast');
	
	var str = '<h2>Emmerproef</h2><p>Om vast te stellen hoe goed de bodem in je tuin het water doorlaat, kun je de emmerproef doen:</p>';
	str += '<p>Je graaft een gat van 30 x 30 x 30 cm en gooit hierin een emmer water leeg. Het is belangrijk om de proef op de plek te doen waar je bijvoorbeeld de infiltratiekratten wilt plaatsen. </p>';
	str += '<ul><li>Als het water binnen een half uur helemaal is weggelopen, heb je een goed doorlatende, zandige grond.</li>';
	str += '<li>Als het meer dan vier uur duurt voordat het water weg is, is de bodem een slecht doorlatende veen- of kleigrond. De bodem is dan minder of niet geschikt voor infiltratie.</li>';
	str += '</ul>';
	jQuery('#gbrww-st-action-overlay-content').html(str);
}

function gbrww_st_close_action_profile_popup(e) {
	
	if (e !== undefined) {
		e.preventDefault();	
	}
	
	jQuery('body').removeClass('gbrww-st_no-scroll');
	
	var overlayElem = jQuery('#gbrww-st-action-overlay');
		
	overlayElem.fadeOut('fast', function() {
		jQuery('#gbrww-st-action-overlay-content').html('');
		jQuery('#gbrww-st-action-overlay-loading').show('');
		
	});
	
}

function gbrww_slide_content_change(args) {

	var sliderElem = jQuery(args.sliderContainerObject[0]);
	var parentElem = sliderElem.parent().parent();

	parentElem.find('.slidenav__button').removeClass('current').attr('aria-current', false);
	parentElem.find('.slidenav__button:eq(' + (args.currentSlideNumber - 1) + ')').addClass('current').attr('aria-current', true);

}

function gbrww_st_setup_image_sliders() {

	var parentElem;

	jQuery('.iosslider').each(function() {

		var sliderElem = jQuery(this);

		if (sliderElem.find('.gbrww__slide').length > 1) {

			parentElem = sliderElem.parent().parent();

			var args = {
				snapToChildren: true,
				desktopClickDrag: true,
				infiniteSlider: true,
				navSlideSelector: parentElem.find('.gbrww__slidenav__button'),
				navPrevSelector: parentElem.find('.gbrww__slider__prev'),
				navNextSelector: parentElem.find('.gbrww__slider__next'),
				onSlideChange: gbrww_slide_content_change,
				onSliderLoaded: gbrww_slide_content_change,
				keyboardControls: true
			}

			sliderElem.iosSlider(args);
		}
	})
}

function gbrww_st_open_action_profile_popup(e) {
	
	e.preventDefault();

	var infoElem = jQuery(this);
	var actionID = parseInt(infoElem.attr('data-actionid'));
	var overlayElem = jQuery('#gbrww-st-action-overlay');

	jQuery('body').addClass('gbrww-st_no-scroll');
	
	overlayElem.fadeIn('fast');
	
	console.log('actionID '+actionID);
    
	var actionProfileInfo = {
	    'action': 'ajax_st_get_action_profile_info',
	    'actionid': actionID
	};

	jQuery.post(gbrww_ajaxURL, actionProfileInfo, function(response) {
		
		var actionInfo = JSON.parse(response);
		
		console.log(actionInfo);

		var relatedList = '';

		if (actionInfo.related.length > 0) {

			relatedList = '<div class="actionoverlay__more"><p><strong>Meer informatie</strong><br>';

			for (var i = 0; i < actionInfo.related.length; i++) {
				relatedList += '<a href="' + actionInfo.related[i][1] + '" >' + actionInfo.related[i][0] + '</a><br>';
			}
			relatedList += '<p></div>';
		}
		
		jQuery('#gbrww-st-action-overlay-loading').hide('');

		jQuery('#gbrww-st-action-overlay-content').html('<h4 class="medium-heading">' + actionInfo.title + '</h4>' + actionInfo.slider + '' + actionInfo.content + relatedList);

		if (actionInfo.slider !== '') {
			gbrww_st_setup_image_sliders();
		}
		
		var focusableElements = overlayElem.find('a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), [tabindex="0"]');
		focusableElements.last().focus();
		
		overlayElem.keydown(function(e) {
		
			if (e.which === 9) { // tab
				
				if (focusableElements.length < 2) {
					
					e.preventDefault();
					
				} else if (e.shiftKey) {
					
					// tab backwards
					
					if (jQuery(document.activeElement) === focusableElements.first()) {
						focusableElements.last().focus();
					}
					
				} else {
					
					// tab forwards
					
					if (jQuery(document.activeElement).html() === focusableElements.last().html()) {
						focusableElements.first().focus();
					}
					
				}
				
			} else if (e.which === 27) { // esc
				
				gbrww_st_close_action_profile_popup();
			}
		
		});

	});
}

function gbrww_st_setup_map() {
	
	if (jQuery('#gbrww-st-map-inner').length) {

		mapboxgl.accessToken = 'pk.eyJ1IjoibGlzYSIsImEiOiJjaW94M2RicTcwMGI5dnBsd2xlNzFkaWdkIn0.AVb35yHkjvGTavohaEinKQ';

		gbrww_st_map = new mapboxgl.Map({
			container: 'gbrww-st-map-inner',
			style: 'mapbox://styles/mapbox/basic-v9',
			center: [4.484406, 51.912552],
			zoom: 14,
			minZoom: 1,
			maxZoom: 19
		});

		gbrww_st_map.addControl(new mapboxgl.NavigationControl());
		
		gbrww_st_map.scrollZoom.disable();

		gbrww_st_map.on('load', function() {
			gbrww_st_get_all_building_labels(false);
		});
		
		if (locations !== undefined && locations.length !== 0) {

		    for (var i = 0; i < locations.length; i++) {

			    if (locations[i].coords !== '') {
					gbrww_st_add_marker(locations[i]);
				}
			}
		}
		
	    jQuery('.klimaatlabel__map__legend__link').click(function(e) {

			e.preventDefault();
		    
		    var thisElem = jQuery(this);
		    var type = thisElem.attr('data-type');
		    
		    var selector = '.marker--'+type;
		    
		    if (thisElem.hasClass('klimaatlabel__map__legend__link--active')) {
			    
			    thisElem.removeClass('klimaatlabel__map__legend__link--active');
			    			    
			    jQuery(selector).addClass('marker--hidden');
			    
		    } else {
			    
			    thisElem.addClass('klimaatlabel__map__legend__link--active');
			    
			    jQuery(selector).removeClass('marker--hidden');
		    }
		    
	    });
	    
    }
}

function gbrww_st_setup() {
	
	gbrww_window_resize();
	gbrww_st_load_actions();
	gbrww_st_setup_map();

	jQuery.each(gbrww_themes, function(index, theme) {

		gbrww_st_themeScores[theme['value']] = 0;
	});

	$('#gbrww-st-action-overlay-close').click(gbrww_st_close_action_profile_popup);
	
	jQuery('#gbrww-st-tool-address-clear').on('click', function(e){
		e.preventDefault();
		gbrww_st_clear_building_info();
	});

	var agree_tooltip = gbrww_st_generate_tooltip(jQuery('#gbrww-st-tool-agree-info').html());
	jQuery('#gbrww-st-tool-agree-info').html(agree_tooltip);

	var shareButton = jQuery('#gbrww-st-tool-result-confirm');

	shareButton.click(function(e) {

		e.preventDefault();

		jQuery('#gbrww-st-share, #gbrww-st-map').show();

		var shareElemPos = jQuery('#gbrww-st-share').offset();
		var scrollTo = shareElemPos.top - jQuery('#gbrww-st-tool-result').outerHeight();

		jQuery('html,body').animate({scrollTop: scrollTo}, 500);
		shareButton.hide();

		gbrww_st_map.resize();

	});

	jQuery('#gbrww-st-tool').on('click', 'a.gbrww-st__measure__info__more', gbrww_st_open_action_profile_popup);
	jQuery('#gbrww-st-action-overlay-close').click(gbrww_st_close_action_profile_popup);

	jQuery('#gbrww-st-tool').on('click', 'a.link_emmerproef', gbrww_st_open_popup_emmerproef);

	jQuery('.gbrww-st__theme__title').click(function(e) {

		e.preventDefault();

		var tabElem = jQuery(this);
		var themeElem = tabElem.parent();

		if (!themeElem.hasClass('gbrww-st__theme--current')) {

			jQuery('.gbrww-st__theme').removeClass('gbrww-st__theme--current');
			themeElem.addClass('gbrww-st__theme--current');
		}

	});

	jQuery('#gbrww-st-searchform').submit(function(e) {
		
		e.preventDefault();
		gbrww_st_search_postcodehouseno(jQuery('#gbrww-st-searchform-postcode').val(), jQuery('#gbrww-st-searchform-houseno').val());
		
	});

	jQuery('body').on('click', '.gbrww-st__checkbox__input', function() {

		var elem = jQuery(this);
		var actionID = parseInt(elem.attr('data-actionid'));

		if (!elem.is(':checked')) {
			gbrww_st_remove_action(actionID);
		} else {
			gbrww_st_add_action(actionID);
		}
		
		gbrww_st_set_building_label_in_db();

	});
	
	jQuery('#gbrww-st-tool-remove-photo').click(gbrww_st_remove_uploaded_photo);

	jQuery('#gbrww-st-saveform').submit(function(e) {

	    e.preventDefault();
	    
        isSet = 1;
        
        var locationIndex = gbrww_st_gbrww_st_get_location_index_by_addressid(addressID);
        
        if (locationIndex !== -1) {
	        locations[locationIndex].title = jQuery('#gbrww-st-saveform-name').val();
        }

		var files = jQuery('#gbrww-st-saveform-upload').prop('files');

		if (files !== undefined && files.length > 0) {

			jQuery('#gbrww-st-saveform-loading').css('opacity', 1);
			jQuery('#gbrww-st-saveform-button').val('Bezig...');

			var file = files[0];

			var formData = new FormData();
			formData.append('photo', file);
			formData.append('action', 'ajax_upload_garden_photo');
			formData.append('addressid', addressID);

			jQuery.ajax({
				url: gbrww_ajaxURL,
				type: 'POST',
				data: formData,
				processData: false,
				contentType: false,
				success: function(response) {

					if (response !== false) {

						jQuery('#gbrww-st-saveform-loading').css('opacity', 0);
						jQuery('#gbrww-st-saveform-button').removeClass('gbrww-st__saveform__button--loading').val('Opslaan');

						var photo = JSON.parse(response);
						addressInfo['photoid'] = photo['id'];
						photoID = photo['id'];
						
						addressInfo.photo = photo['url'];

						var imageElem = jQuery('#gbrww-st-saveform-photo-img');
						imageElem.attr('src', photo['url']);
						imageElem.one('load', function() {
							jQuery('#gbrww-st-saveform-upload').hide();
							jQuery('#gbrww-st-saveform-photo').show();
						});

						gbrww_st_update_marker(photo['url']);
						gbrww_st_scroll_to_map();
						gbrww_st_set_building_label_in_db();

					}
				}
			});

		} else {
			
			gbrww_st_update_marker();
			gbrww_st_scroll_to_map();
			gbrww_st_set_building_label_in_db();
		}
	});
}

function gbrww_show_tooltip() {

	var triggerElem = jQuery(this);
	var contentElem = triggerElem.find('.gbrww-hv__tooltip__content');

	var tooltipElem = jQuery('#gbrww-hv-tooltip');
	var tooltipPos = triggerElem.attr('data-pos');

	gbrww_hv_showTooltip = true;

	tooltipElem.data('pos', tooltipPos).html(contentElem.text()).show();
}

function gbrww_hide_tooltip() {

	gbrww_hv_showTooltip = false;
	jQuery('#gbrww-hv-tooltip').html('').hide();
}

function gbrww_position_tooltip(e) {

    if (gbrww_hv_showTooltip) {

	    var tooltipElem = jQuery('#gbrww-hv-tooltip');
	    var xPos = e.pageX + 12;
	    var yPos = tooltipElem.data('pos') === 'top' ? e.pageY - tooltipElem.outerHeight() - 12 : e.pageY + 12;

	    var rightEdge = gbrww_browserWidth - 10;

	    if (tooltipElem.outerWidth() + xPos > rightEdge) {
		    xPos = rightEdge - tooltipElem.outerWidth();
	    }

		tooltipElem.css('left', xPos).css('top', yPos);

    }
}

function gbrww_window_resize() {
	
	gbrww_get_browser_size();
	gbrww_st_window_resize();
	
	var sliderElem = jQuery('#gbrww-hv-slider');

	if (sliderElem.length) {

		var maxHeight = 0;
		var thisSlideHeight;
		var imageHeight = sliderElem.find('.gbrww-hv__tips__slider__img').first().outerHeight();

		sliderElem.find('.gbrww-hv__tips__slider__info').each(function() {

			thisSlideHeight = jQuery(this).outerHeight();

			if (thisSlideHeight > maxHeight) {
				maxHeight = thisSlideHeight;
			}

		});

		sliderElem.height(maxHeight + imageHeight);
		sliderElem.find('.gbrww-hv__tips__slider__link').height(maxHeight + imageHeight);
	}
	
	
	var maxDescHeight = 'auto';

	if (gbrww_browserWidth >= 850) {

		maxDescHeight = 0;
		var descElemHeight;

		jQuery('.gbrww-st__theme__desc').each(function() {

			descElemHeight = jQuery(this).children('div').outerHeight();

			if (descElemHeight > maxDescHeight) {
				maxDescHeight = descElemHeight;
			}
		});
	}

	jQuery('.gbrww-st__theme__desc').css('height', maxDescHeight);

}

window.addEventListener('resize orientationchange', gbrww_window_resize, false);

jQuery(document).mousemove(function(e) {

	gbrww_position_tooltip(e);
});

jQuery(document).ready(function() {
		
	gbrww_get_browser_size();
	
	if (jQuery('#gbrww-hv-map').length) {
		gbrww_hv_setup();
	}
	
	if (jQuery('#gbrww-hv-tips').length) {
		gbrww_hv_load_tips();
	}
	
	if (jQuery('#gbrww-st-tool').length) {
		gbrww_st_setup();
	}
	
	jQuery('body').append('<div id="gbrww-hv-tooltip" class="gbrww-hv__tooltip"></div>');
	jQuery(document).on('mouseenter', '.gbrww-hv__tooltip__trigger', gbrww_show_tooltip);
	jQuery(document).on('mouseleave', '.gbrww-hv__tooltip__trigger', gbrww_hide_tooltip);
	
});