<?php

/**
 *
 * @since             1.0.2
 * @package           Groenblauw
 *
 * @wordpress-plugin
 * Plugin Name:       Atelier Groenblauw
 * Description:       Integreer de tools van Atelier Groenblauw in de site van Rotterdams WeerWoord
 * Version:           1.0.2
 * Author:            Lisa Dalhuijsen / Studio With
 * Author URI:        https://www.studiowith.nl
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       groenblauw-rww
 */

if (!defined('WPINC')) {
	die;
}

if (!class_exists('Groenblauw')) {
	
	class Groenblauw {
		
		private static $instance = null;
	
		public static function get_instance() {
			if (is_null(self::$instance)) {
				self::$instance = new self;
			}
			return self::$instance;
		}
		
		public function ajax_hv_get_locations() {
			
			$ipAddress = $this->get_ip_address();
					
			$buitenLocations = get_option('groenblauw-rww_hv_koele-buitenplek');
			$binnenLocations = get_option('groenblauw-rww_hv_koele-binnenplek');
			$waterLocations = get_option('groenblauw-rww_hv_waterpunt');
			
			$buitenLocations = $buitenLocations ? $buitenLocations : array();
			$binnenLocations = $binnenLocations ? $binnenLocations : array();
			$waterLocations = $waterLocations ? $waterLocations : array();
						
			foreach ($buitenLocations as &$location) {
				
				$location['title'] = $location['title'] == 'Onbekend' || trim($location['title']) == '' ? 'Koele buitenplek' : $location['title'];
				$location['type'] = array('label' => 'Koele buitenplek', 'value' => 'koele-buitenplek');
				$location['ownrating'] = false;
				
				if (isset($location['ratings'])) {
					
					$sum = 0;
					
					for ($i=0; $i<count($location['ratings']); $i++) {
						
						$sum += $location['ratings'][$i][1];
						
						if ($location['ratings'][$i][0] == $ipAddress) {
							$location['ownrating'] = true;
						}
					}
					
					$location['rating'] = round($sum / count($location['ratings']));
				}
			}
			
			foreach ($binnenLocations as &$location) {
				
				$location['title'] = $location['title'] == 'Onbekend' || trim($location['title']) == '' ? 'Koele binnenplek' : $location['title'];
				$location['type'] = array('label' => 'Koele binnenplek', 'value' => 'koele-binnenplek');
				$location['ownrating'] = false;
				
				if (isset($location['ratings'])) {
					
					$sum = 0;
					
					for ($i=0; $i<count($location['ratings']); $i++) {
						
						$sum += $location['ratings'][$i][1];
						
						if ($location['ratings'][$i][0] == $ipAddress) {
							$location['ownrating'] = true;
						}
					}
					
					$location['rating'] = round($sum / count($location['ratings']));
				}
			}
			
			foreach ($waterLocations as &$location) {
				
				$location['title'] = $location['title'] == 'Onbekend' || trim($location['title']) == '' ? 'Drinkwaterpunt' : $location['title'];
				$location['type'] = array('label' => 'Drinkwaterpunt', 'value' => 'waterpunt');
				$location['ownrating'] = false;
				
				if (isset($location['ratings'])) {
					
					$sum = 0;
					
					for ($i=0; $i<count($location['ratings']); $i++) {
						
						$sum += $location['ratings'][$i][1];
						
						if ($location['ratings'][$i][0] == $ipAddress) {
							$location['ownrating'] = true;
						}
					}
					
					$location['rating'] = round($sum / count($location['ratings']));
				}
			}
			
			wp_die(json_encode(array_merge($buitenLocations, $binnenLocations, $waterLocations)));
		}
		
		public function ajax_hv_get_option() {	
			
			$option = get_option('groenblauw-rww_hv_'.$_REQUEST['type']);
			wp_die(json_encode($option));
		}
		
		public function ajax_hv_set_option() {
			
			$locations = json_decode(stripslashes($_REQUEST['locations']), true);
			
			update_option('groenblauw-rww_hv_'.$_REQUEST['type'], $locations);
			
			wp_die('ajax_hv_set_option done');
		}
		
		public function ajax_hv_load_tips() {
			
			$apiURL = 'https://rotterdamsweerwoord.groenblauwemaatregelen.nl/wp-json/rww/v1/hv-tips';
			$response = wp_remote_get($apiURL);
			$data = wp_remote_retrieve_body($response);
			$tips = json_decode($data);
			
			update_option('groenblauw-rww_hv_tips', $tips);
			
			wp_die($data);
		}
	
		public function hv_get_data_from_groenblauwemaatregelen() {
				
			$apiURL = 'https://rotterdamsweerwoord.groenblauwemaatregelen.nl/wp-json/rww/v1/hv';
			$response = wp_remote_get($apiURL);
			$data = wp_remote_retrieve_body($response);
			
			return json_decode($data);
			
		}
		
		public function hv_get_hittestress() {
				
			$ch = curl_init('https://weerlive.nl/api/weerlive_api_v2.php?key=3410781ec2&locatie=Rotterdam');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			$data = curl_exec($ch);
			$weather = json_decode($data)->liveweer[0];
			curl_close($ch);
			
			date_default_timezone_set("Europe/Amsterdam");
			
			$temp = round(floatval($weather->temp));
			$temp = min($temp, 43); // maximum temperatuur is 43 graden
			$luchtv = round(floatval($weather->lv)/5) * 5; // afronden naar vijftal
			
			$matrix = array(
				0 => array(27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43),
				5 => array(27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43),
				10 => array(27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43),
				15 => array(27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43),
				20 => array(27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43),
				25 => array(27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,44),
				30 => array(27,28,29,30,31,32,33,34,35,36,37,38,40,40,43,46,48),
				35 => array(27,28,29,30,31,32,33,34,35,37,38,40,43,43,47,50,52),
				40 => array(27,28,29,30,31,32,34,35,37,39,41,43,46,47,51,54,56),
				45 => array(27,28,29,30,32,33,35,37,39,41,43,46,49,51,54,57,60),
				50 => array(27,28,30,31,33,34,36,38,41,43,46,49,52,55,58,61,64),
				55 => array(28,29,30,32,34,36,38,40,43,46,48,52,55,59,63,67,71),
				60 => array(28,29,31,33,35,37,40,42,45,48,51,55,59,63,67,71,75),
				65 => array(28,30,32,34,36,39,41,44,48,51,55,59,63,67,71,75,79),
				70 => array(29,31,33,35,38,40,43,47,50,54,58,62,66,70,74,78,82),
				75 => array(29,31,34,36,39,42,46,49,53,58,63,68,73,78,83,88,93),
				80 => array(30,32,35,38,41,44,48,52,57,62,67,72,77,82,87,92,97),
				85 => array(30,33,36,39,43,47,51,55,60,65,70,75,80,85,90,95,100),
				90 => array(31,34,37,41,45,49,54,59,64,69,74,79,84,89,94,99,104),
				95 => array(31,35,38,42,47,51,57,62,67,72,77,82,87,92,97,102,107),
				100 => array(32,36,40,44,49,54,59,64,69,74,79,84,89,94,99,104,109)
			);
			
			if ($temp < 27) {
				$pet = $temp;
			} else {
				$pet = $matrix[$luchtv][$temp-27];
			}
			
			$erIsHittestress = true;
			
			if ($temp < 23) {
				$message = '<span>Geen <br>hittestress</span>';
				$erIsHittestress = false;
			} else if ($temp < 27) {
				$message = '<span class="gbrww-hv__hittestress__title__stress gbrww-hv__hittestress__title__stress--1">Lichte <br>hittestress</span>';
			} else if ($pet < 29) {
				$message = '<span class="gbrww-hv__hittestress__title__stress gbrww-hv__hittestress__title__stress--1">Lichte <br>hittestress</span>';
			} else if ($pet < 35) {
				$message = '<span class="gbrww-hv__hittestress__title__stress gbrww-hv__hittestress__title__stress--2">Hittestress</span>';
			} else if ($pet < 41) {
				$message = '<span class="gbrww-hv__hittestress__title__stress gbrww-hv__hittestress__title__stress--3">Grote <br>hittestress</span>';
			} else {
				$message = '<span class="gbrww-hv__hittestress__title__stress gbrww-hv__hittestress__title__stress--4">Extreme <br>hittestress</span>';
			}

			$result = array(
				'message' => $message,
				'temp' => str_replace('.', ',', $weather->temp),
				'lv' => $weather->lv,
				'gtemp' => str_replace('.', ',', strval($pet)),
				'erIsHittestress' => $erIsHittestress
			);
			
			return $result;
		}
	
		public function hv_process_shortcode($atts) {
			
			wp_enqueue_style('groenblauw-rww');
		    wp_enqueue_script('groenblauw-rww-infobox');
		    wp_enqueue_script('groenblauw-rww-iosslider');
		    //wp_enqueue_script('groenblauw-rww-nlmaps');
		    wp_enqueue_script('groenblauw-rww');
			
			$data = $this->hv_get_data_from_groenblauwemaatregelen();
						
			$ipAddress = $this->get_ip_address();
			
			$hittestress = $this->hv_get_hittestress();
						
			return '<script>
					var gbrww_erIsHittestress = ' . ($hittestress['erIsHittestress'] ? 'true' : 'false') . ';
					var gbrww_ajaxURL = "' . admin_url('admin-ajax.php') . '";
					var gbrww_ipAddress = "' . $ipAddress . '";
				</script>
				
				<div class="gbrww-hv">
					
					<div class="has-background-' . ($hittestress['erIsHittestress'] ? 'orange' : 'blue') . ' is-padding-top-medium is-padding-bottom-medium">
					
						<div class="gbrww-hv__hittestress">
						
							<div class="columns">
					
								<div class="column is-half-tablet">
									
									<h4 class="gbrww-hv__hittestress__label">In Rotterdam is er nu</h4>
									<h3 class="h2 gbrww-hv__hittestress__title">' . $hittestress['message'] . '</h3>
									
								</div>
								
								<div class="column is-half-tablet">
									
									<div class="gbrww-hv__hittestress__data">
										
										<div class="gbrww-hv__hittestress__data__item"><span aria-label="Actuele temperatuur"><span>' . $hittestress['temp'] . '</span>&#x00B0;</span> Actuele temperatuur</div>
											
										<div class="gbrww-hv__hittestress__data__item"><span aria-label="Luchtvochtigheid">' . $hittestress['lv'] . '%</span> Lucht&shy;vochtig&shy;heid</div>
											
										<div class="gbrww-hv__hittestress__data__item"><span aria-label="Gevoelstemperatuur"><span>' . $hittestress['gtemp'] . '</span>&#x00B0;</span> Gevoels&shy;temperatuur <a class="gbrww-hv__tooltip__trigger"><span class="gbrww-hv__tooltip__handle"></span><span class="gbrww-hv__tooltip__content">De gevoelstemperatuur bepaalt de mate van hittestress en hangt van veel gegevens af, waaronder de temperatuur en de luchtvochtigheid. Als de luchtvochtigheid hoog is, is de gevoelstemperatuur hoger.</span></a></div>
										
									</div>
									
									<div class="gbrww-hv__hittestress__credits">KNMI Weergegevens via <a href="http://weerlive.nl/delen.php" target="_blank">Weerlive.nl</a></div>
									
								</div>	
							</div>
						</div>
					</div>
					
					<p>&nbsp;</p>
					
					<div class="columns">
					
						<div class="gbrww-hv__nearest column is-one-third-desktop">
						
							<h3>Vind een koele plek in jouw buurt</h3>
										
							<form id="gbrww-hv-search" action="/" method="post" class="gbrww-hv__searchform">
						
								<div class="gbrww-hv__searchform__group gbrww-hv__searchform__group--left">
						
									<label class="gbrww-hv__searchform__label" for="gbrww-hv-search-postcode">Postcode</label>
									<p id="gbrww-hv-search-error-postcode" class="gbrww-hv__error" role="alert"></p>
									<input id="gbrww-hv-search-postcode" type="text" name="postcode" value="" placeholder="1234AB" autocomplete="postal-code" />
						
								</div><div class="gbrww-hv__searchform__group gbrww-hv__searchform__group--right">
						
									<label class="gbrww-hv__searchform__label" for="gbrww-hv-search-houseno">Huisnummer</label>
									<p id="gbrww-hv-search-error-houseno" class="gbrww-hv__error" role="alert"></p>
									<input id="gbrww-hv-search-houseno" type="text" name="houseno" value="" autocomplete="on" />
						
								</div>
						
								<input type="submit" class="button" value="Vind" />
								
								<p id="gbrww-hv-search-error" class="gbrww-hv__error"></p>
								
							</form>
							
							<p style="margin: 0.5rem 0 1rem 0;">of</p>
							
							<a id="hgbrww-hv-current-location" href="#" class="button"><span class="gbrww-hv__gps-icon"></span>Gebruik huidige locatie</a>
							<p id="hgbrww-hv-current-location-error" class="gbrww-hv__error"></p>

							<p>&nbsp;</p>
							
							<div class="gbrww-hv__nearest gbrww-hv__nearest--buiten">
								<h4>Dichtstbijzijnde koele buitenplekken</h4>
								<ul id="gbrww-hv-nearest-list-buiten" class="gbrww-hv__nearest__list gbrww-hv__nearest__list--buiten"></ul>
							</div>
												
							<div class="gbrww-hv__nearest gbrww-hv__nearest--binnen">
								<h4>Dichtstbijzijnde koele binnenplekken</h4>
								<ul id="gbrww-hv-nearest-list-binnen" class="gbrww-hv__nearest__list gbrww-hv__nearest__list--binnen"></ul>
							</div>
												
							<div class="gbrww-hv__nearest gbrww-hv__nearest--water">
								<h4>Dichtstbijzijnde drinkwaterpunten</h4>
								<ul id="gbrww-hv-nearest-list-water" class="gbrww-hv__nearest__list gbrww-hv__nearest__list--water"></ul>
							</div>
							
						</div>
						
						<div class="gbrww-hv__main column is-two-third-desktop">
							
							<div id="gbrww-hv-map" class="gbrww-hv__map">
							</div>
							
						</div>
						
					</div>
											
					<p>&nbsp;</p>
					
					<div class="has-background-blue is-padding-top-large is-padding-bottom-large">
					
						<div class="inner-wrapper has-text-centered">
							
							<h2 class="h2 has-text-white">Ken jij ook een koele plek in Rotterdam?</h2>
	
							<p><br><a class="button" href="https://rotterdamsweerwoord.nl/heb-je-een-idee/">Laat het ons weten!</a></p>
								
						</div>
					</div>
					
					<p>&nbsp;</p>
					
					<div class="has-background-grey-lighter is-padding-top-large is-padding-bottom-large">
					
						<div class="inner-wrapper">
						
							<div class="columns">
							
								<div class="column">
								
									<h2 class="gbrww-hv__tips__title has-text-centered">Algemene tips om af te koelen</h2>
									
									<div class="gbrww-hv__tips__slider">
						
										<div id="gbrww-hv-slider" class="gbrww-hv__tips__slides">
						
											<div id="gbrww-hv-tips" class="is-clearfix"></div>
											
										</div>
						
										<div class="gbrww-hv__tips__slider__prev" aria-label="vorige" role="button" tabindex="0"></div>
										<div class="gbrww-hv__tips__slider__next" aria-label="volgende" role="button" tabindex="0"></div>
						
									</div>
									
								</div>
							</div>
						</div>
					</div>
					
					<p>&nbsp;</p>
					
					<div class="has-background-blue is-padding-top-large is-padding-bottom-large">
					
						<div class="inner-wrapper has-text-centered">
						
							<div class="columns is-centered">
								
								<div class="column is-four-fifths-tablet">
									
									<h2 class="h2 has-text-white">Houd je huis koel!</h2>
									
									<div class="gbrww-video">
										<iframe src="https://www.youtube.com/embed/yKcvKldBTi8?controls=0&rel=0&modestbranding=1&color=white&disablekb=1" frameborder="0" allowfullscreen></iframe>
									</div>
			
								</div>
							</div>								
						</div>
					</div>	
					
					<p>&nbsp;</p>
					
					<div class="has-background-orange is-padding-top-large is-padding-bottom-large">
					
						<div class="inner-wrapper has-text-centered">
						
							<div class="columns is-centered">
								
								<div class="column is-three-fifths-tablet has-text-white">
									
									<h2 class="h2" style="margin-bottom: 0.5em;">Wat kan jij doen?</h2>
												
									<p>Bekijk de maatregelen om te ontdekken wat jij kunt doen om je huis koel te houden tijdens warme dagen.</p>
			
									<a class="button" href="https://rotterdamsweerwoord.nl/handen-uit-de-mouwen">Handen uit de mouwen</a>
								
								</div>
							</div>								
						</div>
					</div>	
					
					<p>&nbsp;</p>
										
				</div>';
		}
				
		public function st_set_building_label($buildingID, $label, $is_set = 0) {
		
			global $wpdb;
			$table = 'groenblauw_rww_buildings';

			// get building entry from database
		
			$row = $wpdb->get_row('SELECT ID FROM ' . $wpdb->prefix . $table . ' WHERE building_id=' . $buildingID);
		
			if ($row == null) {
		
				// building doesn't exist yet in database
				// insert building
				$b_data = array(
					'building_id' => $buildingID,
					'label' => $label
				);
		
				$wpdb->insert(
					$wpdb->prefix . $table,
					$b_data
				);
		
			} else {
		
				// building already exists in database
				// update entry
				$b_data = array(
					'building_id' => $buildingID,
					'label' => $label
				);
		
				$result = $wpdb->update(
					$wpdb->prefix . $table,
					$b_data,
					array('ID' => $row->ID)
				);
		
			}
		}
				
		public function st_set_addres_info($addressID, $buildingID, $label, $actions, $name, $coords, $postcode, $houseno, $photoid, $isSet) {
		
			global $wpdb;
			$table = 'groenblauw_rww_addresses';
		
			// get address entry from database
		
			$row = $wpdb->get_row('SELECT ID FROM ' . $wpdb->prefix . $table . ' WHERE address_id=' . $addressID);
		
			if ($row == null) {
		
				// address doesn't exist yet in database
				// insert address
		
				$wpdb->insert(
					$wpdb->prefix . $table,
					array(
						'address_id' => $addressID,
						'building_id' => $buildingID,
						'label' => $label,
						'actions' => $actions,
						'name' => $name,
						'coords' => $coords,
						'postcode' => $postcode,
						'houseno' => $houseno,
						'photoid' => $photoid,
						'is_set' => $isSet
					)
				);
		
			} else {
		
				// address already exists in database
				// update entry
		
				$result = $wpdb->update(
					$wpdb->prefix . $table,
					array(
						'address_id' => $addressID,
						'building_id' => $buildingID,
						'label' => $label,
						'actions' => $actions,
						'name' => $name,
						'coords' => $coords,
						'postcode' => $postcode,
						'houseno' => $houseno,
						'photoid' => $photoid,
						'is_set' => $isSet
					),
					array('ID' => $row->ID)
				);
		
			}
		}
		
		public function st_get_address_info_by_id($addressID) {
		
			global $wpdb;
		
			$addressData = $wpdb->get_row('SELECT * FROM ' . $wpdb->prefix . 'groenblauw_rww_addresses WHERE address_id=' . $addressID);
			
			if ($addressData == null) {
		
				$data = '';
		
			} else {
		
				$data = array();
		
				$data['id'] = $addressData->ID;
				$data['label'] = $addressData->label;
				$data['name'] = $addressData->name;
				$data['coords'] = explode(',', $addressData->coords);
				$data['actions'] = $addressData->actions;
				$data['postcode'] = $addressData->postcode;
				$data['houseno'] = $addressData->houseno;
				$data['building_id'] = $addressData->building_id;
				$data['is_set'] = $addressData->is_set;
		
				if ($addressData->photoid == 0 || $addressData->photoid == '') {
		
					$photo = '';
					$photoID = 0;
		
				} else {
					
					$photo = wp_get_attachment_image_src($addressData->photoid, 'medium');
		
					if ($photo) {
		
						$photo = array_shift($photo);
						$photoID = $addressData->photoid;
		
					} else {
		
						$photo = '';
						$photoID = 0;
		
					}
		
				}
		
				$data['photo'] = $photo;
				$data['photoid'] = $photoID;
			}
		
			return $data;
		}
		
		public function st_get_locations() {
			
			$locations = array();
			
			global $wpdb;
			
			$addresses = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'groenblauw_rww_addresses WHERE coords != "0" AND coords IS NOT NULL AND is_set=1');
						
			if ($addresses !== null) {
			
				foreach ($addresses as $address) {
			
					$coords = explode(',', $address->coords);
			
					if (is_array($coords) && count($coords) == 2) {
			
						if ($address->photoid == 0 || $address->photoid == '') {
			
							$photo = '';
			
						} else {
							
							$photo = wp_get_attachment_image_src($address->photoid, 'medium');
							$photo = $photo ? array_shift($photo) : '';
			
						}
						
						$actions = explode(',', $address->actions);
			
						array_push($locations, array(
							'id' => $address->ID,
							'title' => $address->name,
							'photo' => $photo,
							'coords' => $address->coords,
							'label' => $address->label,
							'addressid' => $address->address_id,
							'buildingid' => $address->building_id
						));
					}
				}
			}
			
			return $locations;
		}
		
		public function st_process_shortcode($atts) {
			
			wp_enqueue_style('groenblauw-rww-mapbox');
			wp_enqueue_style('groenblauw-rww');
			wp_enqueue_script('groenblauw-rww-proj4');
			wp_enqueue_script('groenblauw-rww-mapbox');
		    wp_enqueue_script('groenblauw-rww');
			
			$addressInfo = array();
			$allActions = array();
			
			$themeHTML = '';
			
			$themes = array(
			    array(
		            'value' => 'regen',
		            'label' => 'Regen&shy;bestendig'
		        ),
				array(
		            'value' => 'hitte',
		            'label' => 'Minder hitte'
		        ),
		        array(
		            'value' => 'biodiversiteit',
		            'label' => 'Meer bio&shy;diversiteit'
		        )
		    );
			
			foreach($themes as $k => $theme) {
									
				$themeSlug = $theme['value'];
				$themeTitle = $theme['label'];

				$classActive = $k == 0 ? ' gbrww-st__theme--current' : '';

				$moreInfoLink = '';	
								
				$scoreHTML = '';
		
				foreach($themes as $thisTheme) {
									
					$thisThemeSlug = $thisTheme['value'];
					$thisThemeTitle = $thisTheme['label'];	
					$classCurrent = $thisThemeSlug == $themeSlug ? ' gbrww-st__theme__improve__score--current' : '';
					
					$scoreHTML .= '<li class="gbrww-st__theme__improve__score gbrww-st__theme__improve__score--' . $thisThemeSlug . $classCurrent . '" data-theme="' . $thisThemeSlug . '">
						<div class="gbrww-st__theme__improve__score__bar"></div>
						<div class="gbrww-st__theme__improve__score__title">' . $thisThemeTitle . '</div>
						<div class="gbrww-st__theme__improve__score__value">0%</div>
					</li>';
									
				}

				$themeHTML .= '<div id="gbrww-st-theme-' . $themeSlug . '" class="gbrww-st__theme gbrww-st__theme--' . $themeSlug . $classActive . '" data-theme="' . $themeSlug . '">
				
					<a class="gbrww-st__theme__title" href="" data-num="' . $k . '"><span>' . $themeTitle . '</span></a>
				
					<div class="gbrww-st__theme__content">
				
						<div class="gbrww-st__theme__desc">
							<div id="gbrww-st-theme-advice-' . $themeSlug . '"></div>
						</div>
						
						<!--<a class="gbrww-st__theme__more" href="' . $moreInfoLink . '">Meer over dit thema</a>-->
						
						<div class="gbrww-st__theme__improve is-clearfix">
						
							<div class="gbrww-st__theme__improve__measures">
								
								<p>Selecteer de maatregelen die jij al hebt genomen of gaat nemen!</p>
	
								<div id="gbrww-st-actions-' . $themeSlug . '" class="gbrww-st__theme__improve__measures__panel"></div>
						
							</div>
							
							<ul class="gbrww-st__theme__improve__scores">' . $scoreHTML . '</ul>
							
						</div>
					</div>
				</div>';
				
			}
			
			$scaleHTML = '';
			
			foreach(array('g','f','e','d','c','b','a') as $char) {
						
				$scaleHTML .= '<div class="gbrww-st-label__item gbrww-st-label__item--' . $char . '"></div>';
				
			}
						
			$takenActions = array();
			
			$cookie_name = 'gbrww_addressid';
			
			if (isset($_COOKIE[$cookie_name]) && $_COOKIE[$cookie_name] !== '') {
			
				$addressID = $_COOKIE[$cookie_name];
			    $addressInfo = $this->st_get_address_info_by_id($addressID);
			    
				if (isset($addressInfo['coords'])) {
			
					if (isset($addressInfo['actions']) && $addressInfo['actions'] != '') {
			
						$takenActionStrings = explode(',', $addressInfo['actions']);
			
						foreach($takenActionStrings as $string) {
							$takenActions[] = intval($string);
						}
			
					}
			
					$addressInfo['photoid'] = intval($addressInfo['photoid']);
			
				} else {
			
					$addressID = '';
					$addressInfo = array(
						'coords' => '',
						'photoid' => '',
						'photo' => '',
						'postcode' => '',
						'houseno' => '',
			            'building_id' => '',
						'is_set' => 0
					);
					$takenActions = array();
			
				}
			
			} else {
			
				$addressID = '';
				$addressInfo = array(
					'coords' => '',
					'photoid' => '',
					'photo' => '',
					'postcode' => '',
					'houseno' => '',
			        'building_id' => '',
					'is_set' => 0
				);
				$takenActions = array();
			
			}

		
			$html = '<script>
					var locations = ' . json_encode($this->st_get_locations()) . ';
					var gbrww_ajaxURL = "' . admin_url('admin-ajax.php') . '";
					var gbrww_themes = ' . json_encode($themes) . ';
					var takenActions = ' . json_encode($takenActions) . ';
					var photoID = \'' . $addressInfo['photoid'] . '\';
					var isSet = ' . $addressInfo['is_set'] . ';
					var addressInfo = ' . json_encode($addressInfo) . ';
					var addressID = \'' . $addressID . '\';
				    var buildingID = \'' . $addressInfo['building_id'] . '\';
					var currentBuildingCenter = ' . json_encode($addressInfo['coords']) . ';
			</script>
			
			<div class="gbrww-st">
				
				<div id="gbrww-st-search">
	
					<h3>Wat is jouw klimaatlabel?</h3>
		
					<form id="gbrww-st-searchform" class="gbrww-st__searchform" action="/" method="post">
		
						<fieldset class="gbrww-st__searchform__group">
		
							<label for="gm-searchform-postcode">Postcode</label>
							<input id="gbrww-st-searchform-postcode" type="text" name="postcode" value="' . (isset($addressInfo['postcode']) ? $addressInfo['postcode'] : '') . '" placeholder="Postcode" autocomplete="postal-code" required />
		
						</fieldset>
		
						<fieldset class="gbrww-st__searchform__group">
		
							<label for="gm-searchform-houseno">Huisnummer</label>
							<input id="gbrww-st-searchform-houseno" type="text" name="houseno" value="' . (isset($addressInfo['houseno']) ? $addressInfo['houseno'] : '') . '" placeholder="Huisnummer" autocomplete="on" required />
		
						</fieldset>
		
						<button type="submit" class="button">Zoek adres</button>
		
						<p id="gbrww-st-tool-result-error" class="gbrww-st__result__error" role="alert"></p>
		
					</form>
					
					<p id="gbrww-st-searchform-error-postcode" class="gbrww-st__searchform__error" role="alert"></p>
					<p id="gbrww-st-searchform-error-houseno" class="gbrww-st__searchform__error" role="alert"></p>
					<p id="gbrww-st-searchform-error" class="gbrww-st__searchform__error" role="alert"></p>
					
		            <p id="gbrww-st-search-no-result" class="gbrww-st__searchform__error" role="alert">Omdat dit geen woonadres lijkt te zijn hebben we helaas geen informatie beschikbaar voor dit adres.</p>
		
				</div>
								
				<div id="gbrww-st-tool" style="display: none;">
				
					<div>
						<p class="gbrww-st__address">Resultaat voor <strong id="gbrww-st-tool-address">jouw adres</strong> <a href="" id="gbrww-st-tool-address-clear" class="gbrww-st__address-clear">Kies een ander adres</a></p>
			
						<div class="gbrww-st__themes">' . $themeHTML . '</div>
					</div>
			
					<div id="gbrww-st-tool-result" class="gbrww-st__result gbrww-st__result--fixed">
							
						<div class="container">				
							<div>
				
								<p class="gbrww-st__result__text">Jouw klimaatlabel wordt</p>
				
								<div class="gbrww-st__result__label">
				
									<div class="gbrww-st-label">
				
										<div id="gbrww-st-yourlabel" class="gbrww-st-label__char" data-label="g" data-labelnum="6" data-addressid="" data-buildingid=""></div>' . $scaleHTML . '
									</div>
								</div>
				
								<a id="gbrww-st-tool-result-confirm" href="" class="button gbrww-st__result__button">Bevestig<span> je label</span></a>
				
							</div>
						</div>
					</div>
				
					<div id="gbrww-st-share" class="gbrww-st__share" style="display: none;">
					
						<h2 id="tool-congratulations" style="margin-bottom: 1em;">Gefeliciteerd, je bent goed bezig!</h2>
							
						<div class="columns">
			
							<div class="column is-half-tablet">
							
								<div class="gbrww-st__savedesc">
			
									<p class="gbrww-st__savedesc__intro">Inspireer anderen met jouw tuin en deel je resultaat!</p>
			
									<p>Laat zien hoe klimaatbestendig jouw tuin is en zet \'m op de kaart. Samen maken wij onze stad klimaatbestendig!</p>
								</div>
							</div>
							
							<div class="column is-half-tablet">
			
								<form id="gbrww-st-saveform" class="gbrww-st__saveform" action="/" method="post">
			
									<fieldset class="gbrww-st__saveform__group">
			
										<label class="" for="gm-saveform-name">Je naam:</label>
										<p id="gbrww-st-saveform-error-name" class="gbrww-st__searchform__error" role="alert"></p>
										<input id="gbrww-st-saveform-name" type="text" name="name" value="' . (isset($addressInfo['name']) ? $addressInfo['name'] : '') . '" autocomplete="name" />
			
									</fieldset>
			
									<fieldset class="gbrww-st__saveform__group" style="width: 100%;">
			
										<label class="" for="gm-saveform-upload">Upload een foto van je tuin:</label>
										<p id="gbrww-st-saveform-error-photo" class="gbrww-st__searchform__error" role="alert"></p>
			
										<input id="gbrww-st-saveform-upload" type="file" class="gbrww-st__saveform__file" name="photo" value="" accept=".png, .jpeg, .jpg, .gif"' . (isset($addressInfo['photo']) && $addressInfo['photo'] != '' ? ' style="display: none"' : '') . ' />
			
									</fieldset>
			
									<div id="gbrww-st-saveform-photo" class=""' . (isset($addressInfo['photo']) && $addressInfo['photo'] != '' ? '' : ' style="display: none"') . '>
										<img id="gbrww-st-saveform-photo-img" class="gbrww-st__saveform__img" src="' . (isset($addressInfo['photo']) ? $addressInfo['photo'] : '') . '" alt="" />
										<a id="gbrww-st-tool-remove-photo" href="#">Verwijderen</a>
									</div>
			
									<label for="gm-tool-agree" class="gbrww-st__saveform__checkbox"><input type="checkbox" id="gbrww-st-tool-agree" name="agree" required' . (empty($addressInfo['coords']) ? '' : ' checked') . '>
			                            Mijn gegevens mogen op deze website worden bewaard.<a class="gbrww-hv__tooltip__trigger"><span class="gbrww-hv__tooltip__handle"></span><span class="gbrww-hv__tooltip__content">Deze website verwerkt enkel persoonsgegevens wanneer je de scoretool invult, en enkel met betrekking tot het specifieke doel waarvoor de scoretool dient. Je ingevoerde gegevens (postode, huisnummer, zelfgekozen naam en ondernomen maatregelen) worden anoniem opgeslagen en niet met derden gedeeld.
			                            Voor het laten verwijderen van je gegevens of het maken van bezwaar kan je mailen naar info@ateliergroenblauw.nl.</span></a>

			                        </label>
			                        <br>
									<button id="gbrww-st-saveform-button" type="submit" class="gbrww-st__saveform__button button">Opslaan</button>
			
									<div id="gbrww-st-saveform-loading" class="gbrww-st__saveform__loading"></div>
			
								</form>
							</div>
												
						</div>
						
					</div>
				
				</div>
				
				<div id="gbrww-st-map">
						
					<div class="gbrww-st__map">
						<div id="gbrww-st-map-inner"></div>
					</div>
					
				</div>
				
				<p>&nbsp;</p>
				
				<div class="has-background-orange is-padding-top-large is-padding-bottom-large">
				
					<div class="inner-wrapper has-text-centered">
					
						<div class="columns is-centered">
							
							<div class="column is-three-fifths-tablet has-text-white">
								
								<h2 class="h2" style="margin-bottom: 0.5em;">Wat kan jij doen?</h2>
											
								<p>Ontdek wat jij kan doen om de stad regenbestendig, minder warm en natuurlijker te maken.</p>
		
								<a class="button" href="https://rotterdamsweerwoord.nl/handen-uit-de-mouwen">Handen uit de mouwen</a>
							
							</div>
						</div>								
					</div>
				</div>	
				
				<p>&nbsp;</p>								
		
			</div>
			<div id="gbrww-st-action-overlay" class="gbrww-st__actionoverlay" role="dialog">
	
				<div class="gbrww-st__actionoverlay__panel">
	
					<div id="gbrww-st-action-overlay-loading" class="gbrww-st__actionoverlay__loading" role="status">Laden...</div>
	
					<div id="gbrww-st-action-overlay-content" class="gbrww-st__actionoverlay__content"></div>
	
					<a id="gbrww-st-action-overlay-close" class="gbrww-st__actionoverlay__close" aria-label="sluiten" aria-role="button" href="#">×</a>
					
				</div>
			</div>';
		
			return $html;
			
		}
		
		public function ajax_st_load_actions() {
			
			$apiURL = 'https://rotterdamsweerwoord.groenblauwemaatregelen.nl/wp-json/rww/v1/st-actions';
			$response = wp_remote_get($apiURL);
			$data = wp_remote_retrieve_body($response);
						
			wp_die($data);
		}
		
		public function ajax_st_set_address_cookie() {
			
			setcookie('gbrww_addressid', $_REQUEST['addressid'], time() + 999*24*60*60, COOKIEPATH, COOKIE_DOMAIN);
		
			wp_die();
		}

		public function ajax_st_set_address_info() {
		
			global $wpdb;
		
			$label = intval($_REQUEST['label']);
			$addressID = intval($_REQUEST['addressid']);
			$buildingID = intval($_REQUEST['buildingid']);
			$actions = $_REQUEST['actions'];
			$name = $_REQUEST['name'];
			$coords = $_REQUEST['coords'];
			$postcode = $_REQUEST['postcode'];
			$houseno = $_REQUEST['houseno'];
			$photoID = $_REQUEST['photoid'];
			$isSet = intval($_REQUEST['is_set']);
		
			// if there already was a photoid, and the new photoid is different, remove the old one
						
			$addressInfo = $this->st_get_address_info_by_id($addressID);
			$oldPhotoID = $addressInfo['photoid'];
		
			if ($photoID != $oldPhotoID && $oldPhotoID != '' && $oldPhotoID != 0) {
		
				wp_delete_attachment($oldPhotoID, true);
		
			}
		
			$this->st_set_addres_info($addressID, $buildingID, $label, $actions, $name, $coords, $postcode, $houseno, $photoID, $isSet);
		
			// get all addresses in the same building
		
			$addresses = $wpdb->get_results('SELECT label FROM ' . $wpdb->prefix . 'groenblauw_rww_addresses WHERE building_id=' . $buildingID . ' AND is_set=1');
				
			if (!empty($addresses)) {
		
				$total = 0;
		
				foreach ($addresses as $address) {
					$total += $address->label;
				}
		
				// calcultate average label of addresses
		
				$averageLabel = round($total / count($addresses));
		
				// set average label for building
		
				$this->st_set_building_label($buildingID, $averageLabel);
				
			}
			
			wp_die();
			
		}
		
		public function ajax_st_get_address_info_by_id() {
		
			$addressID = intval($_REQUEST['addressid']);
			$addressInfo = $this->st_get_address_info_by_id($addressID);
		
			wp_die($addressInfo == '' ? '' : json_encode($addressInfo));
		}
		
		public function ajax_st_get_all_building_labels() {
		
			global $wpdb;
			
			$buildingLabels = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'groenblauw_rww_addresses');
			
			if ($buildingLabels == null) {
				wp_die();
			} else {
				wp_die(json_encode($buildingLabels));
			}
		}
		
		public function generate_slider($images) {
	
			$html = '<div class="gbrww__fluid-height">
				<div class="gbrww__slider-container">
					<div class="iosslider">
						<div class="gbrww__slider">';
		
			foreach ($images as $img) {
		
				$html .= '<div class="gbrww__slide" style="background-image: url(';
		
				$html .= isset($img->sizes->large) ? $img->sizes->large : $img->url;
		
				$html .= ');">';
//				$html .= generate_credits($img['id'], true, true);
				$html .= '</div>';
		
			}
			
			$html .= '</div>
					</div>
				</div>';
		
			if (count($images) > 1) {
		
				$html .= '<div class="gbrww__slidenav">';
		
			    foreach( $images as $key => $img ) {
		
				    $html .= '<div class="gbrww__slidenav__button" data-index="' . $key . '" aria-label="Slide ' . $key . '" role="button" aria-current="false"></div>';
		
			    }
		
				$html .= '</div>
		
					<div class="gbrww__slider__prevnext">
						<div class="gbrww__slider__prev" aria-label="vorige" role="button" tabindex="0"></div>
						<div class="gbrww__slider__next" aria-label="volgende" role="button" tabindex="0"></div>
					</div>';
			}
		
			$html .= '</div>';
				
			return $html;
		}
		
		public function ajax_st_get_action_profile_info() {
		
			$actionid = intval($_REQUEST['actionid']);
			
			$apiURL = 'https://groenblauwemaatregelen.nl/wp-json/wp/v2/score_actie?include[]=' . $actionid;
			
			$response = wp_remote_get($apiURL);
			$data = wp_remote_retrieve_body($response);
			$action = json_decode($data);
			$action = $action[0];
		
			$content = $action->content->rendered;
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]&gt;', $content);
		
			$photos = $action->acf->action_images;
		
			if (isset($photos) && $photos !== '' && is_array($photos)) {
				$slider = $this->generate_slider($photos);
			} else {
				$slider = '';
			}
		
			$relatedLinks = array();
			
			foreach($action->acf->action_measures_rww as $related) {
				array_push($relatedLinks, array($related->title, $related->url));
			}
		
			
			$info = array(
				'title' => $action->title->rendered,
				'slider' => $slider,
				'content' => $content,
				'related' => $relatedLinks
			);
			
			wp_die(json_encode($info));
		}
		
		public function get_ip_address() {
			
			return isset($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_CLIENT_IP'] : (isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']);
		}
			
		public function enqueue_scripts() {
			
		    wp_register_style('groenblauw-rww', plugin_dir_url( __FILE__ ) . 'assets/css/groenblauw-rww.css');
		    	
			wp_register_style('groenblauw-rww-mapbox', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.49.0/mapbox-gl.css');
			
			if (stripos($_SERVER['HTTP_HOST'], 'local') !== false || stripos($_SERVER['HTTP_HOST'], 'ateliergroenblauw.nl') !== false) {
				
				wp_deregister_script('jquery');
				wp_enqueue_script('groenblauw-rww-jquery', 'https://code.jquery.com/jquery-3.3.1.min.js', false, null, true);
			
			    wp_enqueue_script('groenblauw-rww-google', 'https://maps.googleapis.com/maps/api/js?libraries=geometry&key=AIzaSyCec5nI-LeA3NdQthcK4S3p3kQ70xQ9czM', array(), false, true);
			
			    wp_enqueue_script('groenblauw-rww-google-clusterer', 'https://unpkg.com/@googlemaps/markerclustererplus/dist/index.min.js', array(), false, true);
			    
			    $deps = array('groenblauw-rww-jquery', 'groenblauw-rww-google', 'groenblauw-rww-google-clusterer');
			    
			} else {
				
			    $deps = array('jquery-core', 'pms72_script_map', 'pms72_script_mapclusters');
			}

		    wp_register_script('groenblauw-rww-proj4', 'https://cdnjs.cloudflare.com/ajax/libs/proj4js/2.7.5/proj4.min.js', array(), false, false);
		    wp_register_script('groenblauw-rww-mapbox', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.49.0/mapbox-gl.js', array(), false, false);
			wp_register_script('groenblauw-rww-infobox', plugin_dir_url( __FILE__ ) . 'assets/js/groenblauw-rww-infobox.min.js', $deps, false, true);
			wp_register_script('groenblauw-rww-iosslider', plugin_dir_url( __FILE__ ) . 'assets/js/groenblauw-rww-iosslider.min.js', $deps, false, true);
			//wp_register_script('groenblauw-rww-nlmaps', plugin_dir_url( __FILE__ ) . 'assets/js/groenblauw-rww-nlmaps.iife.js', $deps, false, true);
			wp_register_script('groenblauw-rww', plugin_dir_url( __FILE__ ) . 'assets/js/groenblauw-rww.min.js', $deps, false, true);
				    
		}
		
		public function create_tables() {
			
			global $wpdb;
			
			$charset_collate = $wpdb->get_charset_collate();
		
			$table_name = $wpdb->prefix . 'groenblauw_rww_addresses';
			
			$sql = "CREATE TABLE $table_name (
				ID bigint(20) NOT NULL AUTO_INCREMENT,
				address_id bigint(20) NOT NULL,
				building_id bigint(20) NOT NULL,
				label char(1) NOT NULL,
				actions text NOT NULL,
				name tinytext NOT NULL,
				coords tinytext NOT NULL,
				postcode tinytext NOT NULL,
				houseno tinytext NOT NULL,
				photoid bigint(20) NOT NULL,
				is_set tinyint(1) NOT NULL DEFAULT '1',
				PRIMARY KEY  (ID)
			) $charset_collate;";
		
			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );
		
			$table_name = $wpdb->prefix . 'groenblauw_rww_buildings';
			
			$sql = "CREATE TABLE $table_name (
				ID bigint(20) NOT NULL AUTO_INCREMENT,
				building_id bigint(20) NOT NULL,
				label tinyint(1) DEFAULT NULL,
				is_set tinyint(1) NOT NULL DEFAULT '0',
				PRIMARY KEY (ID)
			) $charset_collate;";
		
			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );
			
		}

		public function ajax_upload_garden_photo() {
		
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			require_once( ABSPATH . 'wp-admin/includes/media.php' );
		
			$photoid = media_handle_upload('photo', 0);
		
			if (is_wp_error($photoid)) {
				
				wp_die(false);
		
			} else {
		
				$photoURL = wp_get_attachment_image_src($photoid, 'medium');
		
				wp_die(json_encode(array(
					'id' => $photoid,
					'url' => $photoURL[0]
				)));
		
			}
		}

		public function __construct() {
			
			register_activation_hook( __FILE__, array($this, 'create_tables') );
		
			add_action('wp_ajax_ajax_hv_load_tips', array($this, 'ajax_hv_load_tips'));
			add_action('wp_ajax_nopriv_ajax_hv_load_tips', array($this, 'ajax_hv_load_tips'));
		
			add_action('wp_ajax_ajax_hv_get_locations', array($this, 'ajax_hv_get_locations'));
			add_action('wp_ajax_nopriv_ajax_hv_get_locations', array($this, 'ajax_hv_get_locations'));
		
			add_action('wp_ajax_ajax_hv_get_option', array($this, 'ajax_hv_get_option'));
			add_action('wp_ajax_nopriv_ajax_hv_get_option', array($this, 'ajax_hv_get_option'));
		
			add_action('wp_ajax_ajax_hv_set_option', array($this, 'ajax_hv_set_option'));
			add_action('wp_ajax_nopriv_ajax_hv_set_option', array($this, 'ajax_hv_set_option'));
		
			add_action('wp_ajax_ajax_st_load_actions', array($this, 'ajax_st_load_actions'));
			add_action('wp_ajax_nopriv_ajax_st_load_actions', array($this, 'ajax_st_load_actions'));
						
			add_action('wp_ajax_ajax_st_get_all_building_labels', array($this, 'ajax_st_get_all_building_labels'));
			add_action('wp_ajax_nopriv_ajax_st_get_all_building_labels', array($this, 'ajax_st_get_all_building_labels'));
			
			add_action('wp_ajax_ajax_st_set_address_cookie', array($this, 'ajax_st_set_address_cookie'));
			add_action('wp_ajax_nopriv_ajax_st_set_address_cookie', array($this, 'ajax_st_set_address_cookie'));
			
			add_action('wp_ajax_ajax_st_get_address_info_by_id', array($this, 'ajax_st_get_address_info_by_id'));
			add_action('wp_ajax_nopriv_ajax_st_get_address_info_by_id', array($this, 'ajax_st_get_address_info_by_id'));
			
			add_action('wp_ajax_ajax_st_set_address_info', array($this, 'ajax_st_set_address_info'));
			add_action('wp_ajax_nopriv_ajax_st_set_address_info', array($this, 'ajax_st_set_address_info'));
			
			add_action('wp_ajax_ajax_st_get_action_profile_info', array($this, 'ajax_st_get_action_profile_info'));
			add_action('wp_ajax_nopriv_ajax_st_get_action_profile_info', array($this, 'ajax_st_get_action_profile_info'));
			
			add_action('wp_ajax_ajax_upload_garden_photo', array($this, 'ajax_upload_garden_photo'));
			add_action('wp_ajax_nopriv_ajax_upload_garden_photo', array($this, 'ajax_upload_garden_photo'));
	
			add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));
			
			add_shortcode('groenblauw_hitteverklikker', array($this, 'hv_process_shortcode'));
			add_shortcode('groenblauw_scoretool', array($this, 'st_process_shortcode'));
		
		}
	}
	
	Groenblauw::get_instance();
}